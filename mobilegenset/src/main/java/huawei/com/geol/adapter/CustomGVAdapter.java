package huawei.com.geol.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import huawei.com.geol.R;
import huawei.com.geol.app.GensetActivity;
import huawei.com.geol.app.MainNew;
import huawei.com.geol.app.OnlineSearch;
import huawei.com.geol.app.RemotePanel;
import huawei.com.geol.app.SiteEntry;
import huawei.com.geol.app.VipSite;

/**
 * Created by Oman on 5/11/2016.
 */

public class CustomGVAdapter extends BaseAdapter {
    private Context mContext;
    private Activity activity;
    private final String[] string;
    private final int[] Imageid;

    public CustomGVAdapter(Activity act, Context c, String[] string, int[] Imageid ) {
        mContext = c;
        this.activity = act;
        this.Imageid = Imageid;
        this.string = string;
    }

    @Override
    public int getCount() {
        return string.length;
    }

    @Override
    public Object getItem(int p) {
        return null;
    }

    @Override
    public long getItemId(int p) {
        return 0;
    }

    @Override
    public View getView(final int p, final View convertView, ViewGroup parent) {
        View grid;
        Intent i;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.gridview_custom_layout, null);
            TextView textView = (TextView) grid.findViewById(R.id.gridview_text);
            ImageView imageView = (ImageView)grid.findViewById(R.id.gridview_image);
            textView.setText(string[p]);
            imageView.setImageResource(Imageid[p]);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i;
                    switch (string[p].toLowerCase()) {
                        case "dashboard":
                            i = new Intent(activity, MainNew.class);
                            activity.startActivity(i);
                            break;
                        case "remote panel":
                            i = new Intent(activity, GensetActivity.class);
                            activity.startActivity(i);
                            break;
                        case "site entry":
                            i = new Intent(activity, SiteEntry.class);
                            activity.startActivity(i);
                            break;
                        case "online search":
                            i = new Intent(activity, OnlineSearch.class);
                            activity.startActivity(i);
                            break;
                        default:
                            Toast.makeText(activity, "Invalid Choice...", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });
        } else {
            grid = (View) convertView;
        }

        return grid;
    }
}