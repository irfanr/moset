package huawei.com.geol.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.List;

import huawei.com.geol.app.DashNav2g;
import huawei.com.geol.app.DashNav3g;
import huawei.com.geol.app.DashNav4g;
import huawei.com.geol.app.Dashboard;

/**
 * Created by adinugroho
 */
public class ChildAdapter extends FragmentPagerAdapter {
    private final List<String> mTitles;

    public ChildAdapter(FragmentManager fm, List<String> titles) {
        super(fm);
        mTitles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Log.v("Child: ", "LTOA");
                return Dashboard.newInstance("LTOA");
            case 1:
                Log.v("Child: ", "NAV2G");
                return DashNav2g.newInstance("NAV2G");
            case 2:
                Log.v("Child: ", "NAV3G");
                return DashNav3g.newInstance("NAV3G");
            case 3:
                Log.v("Child: ", "NAV4G");
                return DashNav4g.newInstance("NAV4G");
            default:
                Log.v("Child: ", "LTOA");
                return new Fragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }
}
