package huawei.com.geol.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import huawei.com.geol.app.DashboardChild;
import huawei.com.geol.app.DumpPage;
import huawei.com.geol.app.MainAlarm;
import huawei.com.geol.app.MainNEDownGIS;

/**
 */
public class ParentAdapter extends FragmentPagerAdapter {
    private final List<String> mTitles;

    public ParentAdapter(FragmentManager fm, List<String> titles) {
        super(fm);
        mTitles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return DashboardChild.newInstance();
        } else if (position == 1) {
            return MainAlarm.newInstance();
        } else if (position == 2) {
            return MainNEDownGIS.newInstance();
        } else {
            return DumpPage.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }
}
