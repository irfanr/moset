package huawei.com.geol.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import huawei.com.geol.R;

/**
 * MainActivity
 */
public class MainNEDown extends Fragment implements NeDownFragment.OnLoadingListener {

    public MainNEDown() {
        // Required empty public constructor
    }

    public static MainNEDown newInstance() {
        return new MainNEDown();
    }

    private static final String TAG = "MainNEDown.class";

    //horizontal progressbar to show while loading..
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.ne_down, container, false);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBarNeDown);

        if (savedInstanceState == null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_nedown, new NeDownFragment())
                    .commit();
        }
        return rootView;
    }

    @Override
    public void onLoadingStarted() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadingFinished() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

}
