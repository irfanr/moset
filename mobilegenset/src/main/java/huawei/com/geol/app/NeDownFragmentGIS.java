package huawei.com.geol.app;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import huawei.com.geol.R;

/**
 * Created by md101 on 9/4/16.
 */

public class NeDownFragmentGIS extends Fragment
//        implements GoogleMap.OnMarkerClickListener,
        implements SwipeRefreshLayout.OnRefreshListener
{
    MapView mMapView;
    private GoogleMap googleMap;
    List<Drawable> icons4g = new ArrayList<>(4);
    List<Drawable> icons3g = new ArrayList<>(4);
    List<Drawable> icons2g = new ArrayList<>(4);
    ProgressBar mProgressBarParent;
    boolean isLoading = false;
    String region = "";
    String site = "";
    String manager_ = "";
    String spv = "";
    Switch aSwitch, bSwitch, cSwitch;
    List<Marker> marker2g = new ArrayList<Marker>();
    List<Marker> marker3g = new ArrayList<Marker>();
    List<Marker> marker4g = new ArrayList<Marker>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    RepoAdapter mRepoAdapter;
    int currentPage = 1;
    final int DEFAULT_PER_PAGE = 1000;
    int perPage = DEFAULT_PER_PAGE;
    String siteid_;
    RelativeLayout frmalarm;
    Button close1;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mProgressBarParent = (ProgressBar) getActivity().findViewById(R.id.progressBarNeDownGIS);
    }

    private static class RepoAdapter extends RecyclerView.Adapter<RepoHolder> {
        LinkedHashMap<Integer, List<APIClient.Alarm>> repoMap;
        List<APIClient.Alarm> repoList;
        FragmentManager fm;

        public RepoAdapter(FragmentManager fm_) {
            repoMap = new LinkedHashMap<Integer, List<APIClient.Alarm>>();
            repoList = new ArrayList<APIClient.Alarm>();
            this.fm = fm_;
        }

        @Override
        public RepoHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            repoList.clear();

            final View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_alarm, viewGroup, false);
            final TextView evID = (TextView) view.findViewById(R.id.txtEventID);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Bundle args = new Bundle();
                    args.putString("eventid", evID.getText().toString());

                    DetailAlarm detailAlarm = new DetailAlarm();
                    detailAlarm.setArguments(args);
                    detailAlarm.show(fm, "");
                    Toast.makeText(view.getContext(), "Getting detail of: " +
                            evID.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
            return new RepoHolder(view);
        }

        @Override
        public void onBindViewHolder(RepoHolder repoHolder, int i) {
            APIClient.Alarm repo = getItem(i);
            if (repo == null) {
                return;
            }
            repoHolder.loadRepo(repo);
        }

        private APIClient.Alarm getItem(int position) {
            int listSize = 0;

            if (repoList.size() > position) {
                return repoList.get(position);
            }

            repoList = new ArrayList<APIClient.Alarm>();
            for (List<APIClient.Alarm> list : repoMap.values()) {
                repoList.addAll(list);
                listSize = listSize + list.size();
                if (listSize > position) {
                    break;
                }
            }
            if (repoList.size() > 0) {
                return repoList.get(position);
            }
            return null;
        }

        @Override
        public int getItemCount() {
            int count = 0;
            for (List<APIClient.Alarm> list : repoMap.values()) {
                count = count + list.size();
            }
            return count;
        }

        public void onNext(List<APIClient.Alarm> repos, int page) {
            if (repos == null) {
                return;
            }
            repoMap.put(page, repos);
            notifyDataSetChanged();
        }
    }

    private static class RepoHolder extends RecyclerView.ViewHolder {
        ImageView imageAvatar;
        TextView alarmname;
        TextView node;
        TextView siteid;
        TextView ttno;
        TextView occurence;
        TextView evID;

        public RepoHolder(View itemView) {
            super(itemView);
            imageAvatar = (ImageView) itemView.findViewById(R.id.imageView_avatar);
            alarmname = (TextView) itemView.findViewById(R.id.textView_alarmname);
            node = (TextView) itemView.findViewById(R.id.textView_node);
            siteid = (TextView) itemView.findViewById(R.id.textView_siteID);
            ttno = (TextView) itemView.findViewById(R.id.textView_TTNO);
            occurence = (TextView) itemView.findViewById(R.id.textView_alarmoccurrence);
            evID = (TextView) itemView.findViewById(R.id.txtEventID);
            ((ImageView) itemView.findViewById(R.id.imageView_triangle)).
                    setColorFilter(itemView
                            .getContext()
                            .getResources()
                            .getColor(R.color.blue_light));

        }

        public void loadRepo(APIClient.Alarm repo) {
            alarmname.setText(repo.alarmname);
            node.setText(repo.node);
            siteid.setText(repo.siteid);
            ttno.setText(repo.ttno);
            occurence.setText(repo.lassoccurence);
            evID.setText(String.valueOf(repo.eventid));

            int iconAvatar;

            switch (repo.severity.toString().toUpperCase()) {
                case "MAJOR":
                    iconAvatar = R.drawable.major;
                    break;
                case "MINOR":
                    iconAvatar = R.drawable.minor;
                    break;
                default:
                    iconAvatar = R.drawable.critical;
                    break;
            }

            Picasso.with(imageAvatar.getContext())
                    .load(iconAvatar)
                    .resize(200, 200)
                    .error(R.drawable.ic_github_placeholder)
                    .placeholder(R.drawable.ic_github_placeholder)
                    .centerCrop()
                    .into(imageAvatar);
        }
    }

    private void loadData(final int page, final int perPage, final String siteid_) {
        new AsyncTask<Integer, Void, List<APIClient.Alarm>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                onLoadingStarted();
            }

            @Override
            protected List<APIClient.Alarm> doInBackground(Integer... params) {
                isLoading = true;
                try {
                    return APIClient.getClient()
                            .searchAlarm(
                                    perPage,
                                    params[0],
                                    "LastOccurrence",
                                    siteid_,
                                    "",
                                    "",
                                    "",
                                    region,
                                    "desc").rows;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.Alarm> repos) {
                isLoading = false;
                if (repos != null) {
                    mRepoAdapter.onNext(repos, page);
                } else {
//                    mRepoAdapter.notifyDataSetChanged();
                }

                onLoadingFinished();

                currentPage = page;
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }.execute(page);
    }

    @Override
    public void onRefresh() {
//        mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        loadData(1, perPage, siteid_);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ne_down_gis, container, false);
        frmalarm = (RelativeLayout) rootView.findViewById(R.id.frmDetail);

        close1 = (Button) rootView.findViewById(R.id.ClickClose);
        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseAlarm();
            }
        });

        frmalarm.setVisibility(View.GONE);

        // for detail alarms
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setEnabled(false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewAlarm);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL,
                false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mRepoAdapter = new RepoAdapter(getActivity().getFragmentManager()));
        // end of

        aSwitch = (Switch) rootView.findViewById(R.id.switch1);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(marker2g, true);
                } else {
                    reDrawLogicalMarkers(marker2g, false);
                }
            }
        });

        bSwitch = (Switch) rootView.findViewById(R.id.switch2);
        bSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(marker3g, true);
                } else {
                    reDrawLogicalMarkers(marker3g, false);
                }
            }
        });

        // switch down
        cSwitch = (Switch) rootView.findViewById(R.id.switch3);
        cSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(marker4g, true);
                } else {
                    reDrawLogicalMarkers(marker4g, false);
                }
            }
        });

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMapToolbarEnabled(false);

                // For dropping a marker at a point on the Map
                LatLng KoKas = new LatLng(-6.224220, 106.842242);

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(KoKas).zoom(9).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        String snip = marker.getSnippet().toString();
//                        mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
                        Toast.makeText(getActivity(), snip.split(",")[0].split(":")[1].trim(), Toast.LENGTH_LONG).show();
                        loadData(1, perPage, snip.split(",")[0].split(":")[1].trim());
                        frmalarm.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

        if (getArguments() != null) {
            Bundle args = getArguments();
            region = args.getString("region");
            site = args.getString("site");
            manager_ = args.getString("manager");
            spv = args.getString("spv");
            loadMarkers(region, site, manager_, spv);
        } else {
            region = "";
            site = "";
            manager_ = "";
            spv = "";
            onLoadingFinished();
        }

        return rootView;
    }

    public void CloseAlarm() {
        mRepoAdapter.repoList.clear();
        mRepoAdapter.notifyDataSetChanged();
        loadData(1, 1000, "XXXXXXXX");
        frmalarm.setVisibility(View.GONE);
    }

    private void reDrawLogicalMarkers(List<Marker> markers, boolean isHide) {
        for (Marker item : markers) {
            item.setVisible(isHide);
        }
    }

    private void callAlarmNetcool() {
        // call fragment and pass data.
        Bundle bundle = new Bundle();
        bundle.putString("myString", "value");
        // set Fragment class Arguments
        AlarmFragment myFragment= new AlarmFragment();
        myFragment.setArguments(bundle);
        // launch fragment
    }

//    @Override
//    public boolean onMarkerClick(Marker marker) {
////        callAlarmNetcool();
////        return false;
//    }

    private void setViewMarkers(LatLng lng) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(lng).zoom(9).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void createIcon() {
        //Icons Index 0=alarm, 1=down, 2=normal
        icons2g.add(getResources().getDrawable(R.drawable.ic_alarm_2g_new));
        icons3g.add(getResources().getDrawable(R.drawable.ic_alarm_3g_new));
        icons4g.add(getResources().getDrawable(R.drawable.ic_alarm_4g_new));
        icons2g.add(getResources().getDrawable(R.drawable.ic_down_2g_new));
        icons3g.add(getResources().getDrawable(R.drawable.ic_down_3g_new));
        icons4g.add(getResources().getDrawable(R.drawable.ic_down_4g_new));
        icons2g.add(getResources().getDrawable(R.drawable.ic_normal_2g_new));
        icons3g.add(getResources().getDrawable(R.drawable.ic_normal_3g_new));
        icons4g.add(getResources().getDrawable(R.drawable.ic_normal_4g_new));
    }

    public void onLoadingStarted() {
        mProgressBarParent.setVisibility(View.VISIBLE);
    }

    public void onLoadingFinished() {
        mProgressBarParent.setVisibility(View.INVISIBLE);
    }

    private void loadMarkers(final String region, final String site, final String manager, final String spv) {
        new AsyncTask<Integer, Void, List<APIClient.NEDown>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                onLoadingStarted();
            }

            @Override
            protected List<APIClient.NEDown> doInBackground(Integer... params) {
                isLoading = true;
                try {
                    if (site.isEmpty() || site.equalsIgnoreCase("")) {
                        return APIClient.getJavaClient()
                                .searchnedown2(region.toUpperCase(), manager, spv).nedown2;
                    } else {
                        return APIClient.getJavaClient()
                                .searchnedownsiteid(site.toUpperCase()).nedown;
                    }
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.NEDown> repos) {
                if (repos == null) {
                    Toast.makeText(
                            getActivity().getApplicationContext(),
                            "Error getting data Site GIS...",
                            Toast.LENGTH_LONG).show();
                }
                isLoading = false;
                onLoadingFinished();
                DrawMarkers(repos);
            }
        }.execute(1);
    }

    private void DrawMarkers(List<APIClient.NEDown> repos) {
        if (repos != null) {
            LatLng repo_latlng = new LatLng(-6.224220, 106.842242);
            for (Iterator<APIClient.NEDown> iter = repos.iterator(); iter.hasNext(); ) {
                APIClient.NEDown repo = iter.next();
                repo_latlng = new LatLng(repo.lat, repo.lon);

                int iconAvatar = R.drawable.ic_down_4g_new;
                switch (repo.ne.toString().toUpperCase()) {
                    case "2G":
                        switch (repo.agingCategory.toString().toUpperCase()) {
                            case "0 - 4 HOURS":
                                iconAvatar = R.drawable.ic_yellow_2g_new;
                                break;
                            case "4 - 24 HOURS":
                                iconAvatar = R.drawable.ic_yellow_2g_new;
                                break;
                            case "1 - 3 DAYS":
                                iconAvatar = R.drawable.ic_alarm_2g_new;
                                break;
                            case "3 - 7 DAYS":
                                iconAvatar = R.drawable.ic_down_2g_new;
                                break;
                            case "> 7 DAYS":
                                iconAvatar = R.drawable.ic_down_2g_new;
                                break;
                        }
                        break;
                    case "3G":
                        switch (repo.agingCategory.toString().toUpperCase()) {
                            case "0 - 4 HOURS":
                                iconAvatar = R.drawable.ic_yellow_3g_new;
                                break;
                            case "4 - 24 HOURS":
                                iconAvatar = R.drawable.ic_yellow_3g_new;
                                break;
                            case "1 - 3 DAYS":
                                iconAvatar = R.drawable.ic_alarm_3g_new;
                                break;
                            case "3 - 7 DAYS":
                                iconAvatar = R.drawable.ic_down_3g_new;
                                break;
                            case "> 7 DAYS":
                                iconAvatar = R.drawable.ic_down_3g_new;
                                break;
                        }
                        break;
                    case "4G":
                        switch (repo.agingCategory.toString().toUpperCase()) {
                            case "0 - 4 HOURS":
                                iconAvatar = R.drawable.ic_yellow_4g_new;
                                break;
                            case "4 - 24 HOURS":
                                iconAvatar = R.drawable.ic_yellow_4g_new;
                                break;
                            case "1 - 3 DAYS":
                                iconAvatar = R.drawable.ic_alarm_4g_new;
                                break;
                            case "3 - 7 DAYS":
                                iconAvatar = R.drawable.ic_down_4g_new;
                                break;
                            case "> 7 DAYS":
                                iconAvatar = R.drawable.ic_down_4g_new;
                                break;
                        }
                        break;
                    default:
                        iconAvatar = R.drawable.ic_down_4g_new;
                        break;
                }

                String agingdata = "";
                switch (repo.agingCategory.toString().toUpperCase()) {
                    case "0 - 4 HOURS":
                        agingdata = "1 DAY";
                        break;
                    case "4 - 24 HOURS":
                        agingdata = "1 DAY";
                        break;
                    case "1 - 3 DAYS":
                        agingdata = repo.agingCategory.toString().toUpperCase();
                        break;
                    case "3 - 7 DAYS":
                        agingdata = repo.agingCategory.toString().toUpperCase();
                        break;
                    case "> 7 DAYS":
                        agingdata = repo.agingCategory.toString().toUpperCase();
                        break;
                }

                Marker marker = googleMap.addMarker(new MarkerOptions()
                        .position(repo_latlng)
                        .title(repo.site)
                        .snippet("Site ID: " + repo.siteid + ", Aging: " + agingdata)
                        .icon(BitmapDescriptorFactory.fromResource(iconAvatar)));

                switch (repo.ne.toString().toUpperCase()) {
                    case "2G":
                        marker2g.add(marker);
                        break;
                    case "3G":
                        marker3g.add(marker);
                        break;
                    case "4G":
                        marker4g.add(marker);
                        break;
                    default:
                        break;
                }

            }
            setViewMarkers(repo_latlng);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
