package huawei.com.geol.app;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import huawei.com.geol.R;

/**
 * Created by md101 on 9/3/16.
 */

public class DetailAlarm extends DialogFragment {
    Button mButton;
    EditText mEditText;
    onSubmitListener mListener;
    String text = "";
    Dialog dialog = null;
    private EditText Output;
    private int year;
    private int month;
    private int day;
    private String eventid;
    static final int DATE_PICKER_ID = 1111;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    private InputMethodManager im;
    TextView alarmname, alarmsummary, alarmeventid, lastoccur, ttno;

    interface onSubmitListener {
        void setOnSubmitListener(String sev, String siteid, String ttno, String lastoccur, String region);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.detail_alarm);
        dialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();

        alarmname = (TextView) dialog.findViewById(R.id.alarmdetail_txtAlarmName);
        alarmeventid = (TextView) dialog.findViewById(R.id.alarmdetail_txtEventID);
        alarmsummary = (TextView) dialog.findViewById(R.id.alarmdetail_txtSummary);
        lastoccur = (TextView) dialog.findViewById(R.id.alarmdetail_txtLastOccur);
        ttno = (TextView) dialog.findViewById(R.id.alarmdetail_txtTTNo);

        if(getArguments() != null) {
            Bundle args = getArguments();
            eventid = args.getString("eventid");
        } else {
            eventid = "";
        }

        loadDetailAlarm(eventid);

        return dialog;
    }

    private void loadDetailAlarm(final String eventid) {
        new AsyncTask<Integer, Void, List<APIClient.AlarmDetail>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected List<APIClient.AlarmDetail> doInBackground(Integer... params) {
                try {
                    return APIClient.getJavaClient()
                            .getAlarmDetail(eventid).detail;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.AlarmDetail> repos) {
                if (repos != null) {
                    for(APIClient.AlarmDetail item : repos){
                        alarmeventid.setText(String.valueOf(item.eventId));
                        alarmname.setText(item.alarmName);
                        alarmsummary.setText(item.summary);
                        lastoccur.setText(item.lastOccurrence);
                        ttno.setText(item.ttno);
                    }
                } else {

                }
            }
        }.execute(1);
    }
}