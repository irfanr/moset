package huawei.com.geol.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.stephentuso.welcome.WelcomeScreenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import huawei.com.geol.R;
import huawei.com.geol.adapter.CustomGVAdapter;

public class MainMenu extends AppCompatActivity {
    String IMEI = "";
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayoutAndroid;
    CoordinatorLayout rootLayoutAndroid;
    GridView gridView;
    Context context;
    ArrayList arrayList;
    Handler mHandler = new Handler();
    Runnable refresh;
    TextView lastupdatedata;
    WelcomeScreenHelper welcomeScreen;
    int backpress;
    ProgressDialog mProgressDialog;

    String khusus = "1";

    public static String[] gridViewStrings = {
            "Dashboard",
            "Remote Panel",
//            "Site Entry",
//            "Online Search"
    };

    public static int[] gridViewImages = {
            R.drawable.dashboard_icon,
            R.drawable.ic_settings_remote_black_24dp
//            R.drawable.bts,
//            R.drawable.areasearch
    };

    public static int[] gridViewImages2 = {
            R.drawable.dashboard_icon,
            R.drawable.viproute
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        backpress = 0;
//        GetIMEI();
//        CheckIMEIKhusus(IMEI, MainMenu.this, this.getApplicationContext());

        welcomeScreen = new WelcomeScreenHelper(this, Welcome.class);
        welcomeScreen.show(savedInstanceState);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        gridView = (GridView) findViewById(R.id.grid);

//        if (khusus.equalsIgnoreCase("1"))
//            gridView.setAdapter(new CustomGVAdapter(this, this, gridViewStrings2, gridViewImages2));
//        else
            gridView.setAdapter(new CustomGVAdapter(this, this, gridViewStrings, gridViewImages));

        lastupdatedata = (TextView) findViewById(R.id.txtLastData);

        this.mHandler = new Handler();
        this.mHandler.postDelayed(m_Runnable, 300000);

//        getDataVersion();

        initInstances();

//        String AppV = getAppVersion();
//        getVersion(AppV);
    }

//    private void GetIMEI() {
//        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
////        IMEI = telephonyManager.getDeviceId();
//    }

//    private void CheckIMEIKhusus(final String data, final Activity activity, final Context context) {
//        new AsyncTask<Integer, Void, String>() {
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            protected String doInBackground(Integer... params) {
//                try {
//                    return APIClient
//                            .getJavaClient()
//                            .authimeikhusus(data)
//                            .user_khusus;
//                } catch (Exception ex) {
//                    return "";
//                }
//            }
//
//            @Override
//            protected void onPostExecute(String repos) {
//                khusus = repos;
//                if (khusus.equalsIgnoreCase("1"))
//                    gridView.setAdapter(new CustomGVAdapter(activity, context, gridViewStrings2, gridViewImages2));
//                else
//                    gridView.setAdapter(new CustomGVAdapter(activity, context, gridViewStrings, gridViewImages));
//            }
//        }.execute(1);
//    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            backpress++;

            // do something
            if (backpress < 2)
                Toast.makeText(MainMenu.this, "Press back again to exit...", Toast.LENGTH_SHORT).show();

            if (backpress >= 2)
                finish();

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initInstances() {
        rootLayoutAndroid = (CoordinatorLayout) findViewById(R.id.android_coordinator_layout);
        collapsingToolbarLayoutAndroid = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_android_layout);
        collapsingToolbarLayoutAndroid.setTitle("m-Genset v1.0");
        collapsingToolbarLayoutAndroid.setCollapsedTitleGravity(View.TEXT_ALIGNMENT_GRAVITY);
    }

    public String getAppVersion() {
        String version = "1.0";
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }

    private final Runnable m_Runnable = new Runnable() {
        public void run() {
//            getDataVersion();
            MainMenu.this.mHandler.postDelayed(m_Runnable, 300000);
        }
    };

    private void getDataVersion() {
        new AsyncTask<Integer, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Integer... params) {
                try {
                    return APIClient
                            .getClient()
                            .getDataVersion()
                            .RECEIVED_TIME;
                } catch (Exception ex) {
                    return "01-01-1990";
                }
            }

            @Override
            protected void onPostExecute(String repos) {
                lastupdatedata.setText("Last Received Data: " + repos);
            }
        }.execute(1);
    }

    private void getVersion(final String appV) {
        new AsyncTask<Integer, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Integer... params) {
                try {
                    return APIClient
                            .getJavaClient()
                            .getVersion(appV)
                            .need_update;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String repos) {
                if (repos != null) {
                    if (repos.equalsIgnoreCase("1"))
                        DisplayPopupUpdate();
                }
            }
        }.execute(1);
    }

    public void DisplayPopupUpdate() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Information");
        builder.setMessage("m-Geol new version is available. Update Now?");

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SetProgressBarDownload();
//                updateAPK();
            }
        }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void SetProgressBarDownload() {
        mProgressDialog = new ProgressDialog(MainMenu.this);
        mProgressDialog.setMessage("Downloading update...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        final DownloadTask downloadTask = new DownloadTask(MainMenu.this);
        downloadTask.execute("http://116.66.203.240:8889/apk/geol-debug.apk");

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });
    }

    public void updateAPK() {
        Toast.makeText(MainMenu.this, "Downloading m-Geol updates...", Toast.LENGTH_LONG).show();

        //get destination to update file and set Uri
        //TODO: First I wanted to store my update .apk file on internal storage for my app but apparently android does not allow you to open and install
        //aplication with existing package from there. So for me, alternative solution is Download directory in external storage. If there is better
        //solution, please inform us in comment
        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        String fileName = "geol-debug.apk";
        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);

        //Delete update file if exists
        File file = new File(destination);
        if (file.exists())
            //file.delete() - test this, I think sometimes it doesnt work
            file.delete();

        //get url of app on server
        String url = MainMenu.this.getString(R.string.update_app_url);

        //set downloadmanager
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(MainMenu.this.getString(R.string.notification_description));
        request.setTitle(MainMenu.this.getString(R.string.app_name));

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                Intent install = new Intent(Intent.ACTION_VIEW);
                install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                install.setDataAndType(uri,
                        manager.getMimeTypeForDownloadedFile(downloadId));
                startActivity(install);

                unregisterReceiver(this);
                finish();
            }
        };
        //register receiver for when .apk download is compete
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void installAPK() {
        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        String fileName = "geol-debug.apk";
        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);

        System.out.println("Destination Open: " + destination);

        File pdfFile = new File(destination);
        Uri path = Uri.fromFile(pdfFile);

        Intent promptInstall = new Intent(Intent.ACTION_VIEW)
                .setDataAndType(Uri.parse("file://" + destination),
                        "application/vnd.android.package-archive");
//        startActivity(promptInstall);

//        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
//        pdfIntent.setDataAndType(path, "application/vnd.android.package-archive");
//        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        try {
            startActivity(promptInstall);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(MainMenu.this, "Err. Installing APK", Toast.LENGTH_SHORT).show();
        }
    }

    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
            String fileName = "geol-debug.apk";
            destination += fileName;

            final Uri uri = Uri.parse("file://" + destination);

            File file = new File(destination);
            if (file.exists())
                file.delete();

            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null)
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            else {
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
                installAPK();
            }
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();

                String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
                String fileName = "geol-debug.apk";
                destination += fileName;

                System.out.println("Destination Downloads: " + destination);

                output = new FileOutputStream(destination);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }
    }
}
