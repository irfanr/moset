package huawei.com.geol.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import gr.net.maroulis.library.EasySplashScreen;
import huawei.com.geol.R;

public class SplashGeol extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EasySplashScreen config = new EasySplashScreen(SplashGeol.this)
                .withFullScreen()
                .withTargetActivity(AuthActivity.class)
                .withSplashTimeOut(2000)
                .withBackgroundResource(R.color.teal_background)
                .withFooterText("Copyright 2018")
                .withBeforeLogoText("Mobile Geographic Online Genset (m-Genset)\n")
                .withLogo(R.mipmap.ic_launcher);
//                .withAfterLogoText("\n");


        //set your own animations
        myCustomTextViewAnimation(config.getFooterTextView());

        //create the view
        View easySplashScreenView = config.create();

        setContentView(easySplashScreenView);
    }

    private void myCustomTextViewAnimation(TextView tv){
        Animation animation=new TranslateAnimation(0,0,480,0);
        animation.setDuration(1200);
        tv.startAnimation(animation);
    }
}
