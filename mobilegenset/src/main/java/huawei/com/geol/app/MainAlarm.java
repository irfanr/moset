package huawei.com.geol.app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import huawei.com.geol.R;
import huawei.com.geol.app.SearchAlarm.onSubmitListener;

/**
 * MainActivity
 */
public class MainAlarm extends Fragment implements
        AlarmFragment.OnLoadingListener,
        onSubmitListener {

    public MainAlarm() {
        // Required empty public constructor
    }

    public static MainAlarm newInstance() {
        return new MainAlarm();
    }

    private static final String TAG = "MainAlarm.class";

    //horizontal progressbar to show while loading..
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.alarm, container, false);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBarAlarm);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchAlarm fragment1 = new SearchAlarm();
                fragment1.mListener = MainAlarm.this;
                fragment1.show(getActivity().getFragmentManager(), "");
            }
        });

        if (savedInstanceState == null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_alarm, new AlarmFragment())
                    .commit();
        }
        return rootView;
    }

    @Override
    public void setOnSubmitListener(String sev, String siteid, String ttno, String lastoccur, String region) {
        Bundle args = new Bundle();
        args.putString("sev", sev);
        args.putString("siteid", siteid);
        args.putString("ttno", ttno);
        args.putString("lastoccur", lastoccur);
        args.putString("region", region);
        AlarmFragment alarmfragment = new AlarmFragment();
        alarmfragment.setArguments(args);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_alarm, alarmfragment)
                .commit();
    }

    @Override
    public void onLoadingStarted() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadingFinished() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

}
