package huawei.com.geol.app;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.common.api.Api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import huawei.com.geol.R;

import static huawei.com.geol.utils.ShareUtils.AlertMsg;

/**
 * Created by md101 on 9/3/16.
 */

public class SearchNEDown extends DialogFragment {
    Button mButton, dButton;
    EditText mEditText;
    onSubmitListener mListener;
    String text = "";
    Dialog dialog = null;
    private EditText Output;
    private int year;
    private int month;
    private int day;
    static final int DATE_PICKER_ID = 1111;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    private InputMethodManager im;

    interface onSubmitListener {
        void setOnSubmitListener(String region, String siteid, String manager, String spv);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.search_ne_down);
        dialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
        mButton = (Button) dialog.findViewById(R.id.button1);
        dButton = (Button) dialog.findViewById(R.id.buttonD);
        dButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Spinner spinnerx = (Spinner) dialog.findViewById(R.id.spin_nedown);
                    Spinner spinner2x = (Spinner) dialog.findViewById(R.id.spin_manager);
                    Spinner spinner3x = (Spinner) dialog.findViewById(R.id.spin_spv);
                    String spindatax = spinnerx.getSelectedItem().toString();
                    String spindata2x = spinner2x.getSelectedItem().toString();
                    String spindata3x = spinner3x.getSelectedItem().toString();
                    if (spindatax.toLowerCase().equalsIgnoreCase("all")) {
                        spindatax = "";
                    }
                    if (spindata2x.toLowerCase().equalsIgnoreCase("all")) {
                        spindata2x = "";
                    }
                    if (spindata3x.toLowerCase().equalsIgnoreCase("all")) {
                        spindata3x = "";
                    }
                    Uri uri = Uri.parse("http://116.66.203.240:8888/geolws/query?n=nedown&param1=" + spindatax + "&param2=" + spindata2x + "&param3=" + spindata3x);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (Exception e) {
                }
            }
        });
        mButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String spindata = "";
                String spindata2 = "";
                String spindata3 = "";
                String site = "";
                try {
                    Spinner spinner = (Spinner) dialog.findViewById(R.id.spin_nedown);
                    Spinner spinner2 = (Spinner) dialog.findViewById(R.id.spin_manager);
                    Spinner spinner3 = (Spinner) dialog.findViewById(R.id.spin_spv);

                    EditText siteid = (EditText) dialog.findViewById(R.id.txt_SiteID);
                    site = siteid.getText().toString().trim();
                    spindata = spinner.getSelectedItem().toString();
                    spindata2 = spinner2.getSelectedItem().toString();
                    spindata3 = spinner3.getSelectedItem().toString();

                    if (spindata.toLowerCase().equalsIgnoreCase("all")) {
                        spindata = "";
                    }
                    if (spindata2.toLowerCase().equalsIgnoreCase("all")) {
                        spindata2 = "";
                    }
                    if (spindata3.toLowerCase().equalsIgnoreCase("all")) {
                        spindata3 = "";
                    }
                } catch (Exception e) {
                }

                mListener.setOnSubmitListener(spindata, site, spindata2, spindata3);
                dismiss();
            }
        });

        InitSpin();
        InitManagers();
        InitSupervisors();
        return dialog;
    }

    private void InitSpin() {
        Spinner spinner = (Spinner) dialog.findViewById(R.id.spin_nedown);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.region, R.layout.spinner_alarm);
        adapter.setDropDownViewResource(R.layout.spinner_alarm);
        spinner.setAdapter(adapter);
    }

    private void InitManagers() {
        final Spinner spinner_mgrs = (Spinner) dialog.findViewById(R.id.spin_manager);
        new AsyncTask<Integer, Void, List<APIClient.mgr>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected List<APIClient.mgr> doInBackground(Integer... params) {
                try {
                    return APIClient
                            .getJavaClient()
                            .getAllMGR()
                            .mgrs;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.mgr> repos) {
                if (repos == null) {

                } else {
                    final ArrayList<String> mgrs = new ArrayList<>();
                    for (APIClient.mgr item : repos) {
                        mgrs.add(item.area_manager);
                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                            getActivity(),
                            R.layout.layout_remarks,
                            mgrs);
                    dataAdapter.setDropDownViewResource(R.layout.listremarkcats);
                    spinner_mgrs.setAdapter(dataAdapter);
                }

            }
        }.execute(1);
    }

    private void InitSupervisors() {
        final Spinner spinner_spvs = (Spinner) dialog.findViewById(R.id.spin_spv);
        new AsyncTask<Integer, Void, List<APIClient.spv>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected List<APIClient.spv> doInBackground(Integer... params) {
                try {
                    return APIClient
                            .getJavaClient()
                            .getAllSPV()
                            .spvs;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.spv> repos) {
                if (repos == null) {

                } else {
                    final ArrayList<String> spvs = new ArrayList<>();
                    for (APIClient.spv item : repos) {
                        spvs.add(item.supervisor_area);
                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                            getActivity(),
                            R.layout.layout_remarks,
                            spvs);
                    dataAdapter.setDropDownViewResource(R.layout.listremarkcats);
                    spinner_spvs.setAdapter(dataAdapter);
                }

            }
        }.execute(1);
    }

}