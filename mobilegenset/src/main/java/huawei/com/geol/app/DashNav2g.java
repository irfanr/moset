package huawei.com.geol.app;

/**
 * Created by md101 on 8/9/16.
 */

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;
import huawei.com.geol.R;
import huawei.com.geol.chart.MyMarkerView;
import huawei.com.geol.utils.HttpUtils;

import static huawei.com.geol.constant.constant.LOG_TAG;

public class DashNav2g extends Fragment
        implements OnChartGestureListener, OnChartValueSelectedListener {
    AsyncHttpClient client = new AsyncHttpClient();

    /*
    * Set Of LTOA Chart Data
    * */
    ArrayList<String> x_ltoa = new ArrayList<String>();
    ArrayList<Entry> ltoa_jabo1 = new ArrayList<Entry>();
    ArrayList<Entry> ltoa_central = new ArrayList<Entry>();
    ArrayList<Entry> ltoa_east = new ArrayList<Entry>();
    ArrayList<Entry> ltoa_north = new ArrayList<Entry>();
    ArrayList<Entry> ltoa_nw = new ArrayList<Entry>();
    ArrayList<Entry> ltoa_sw = new ArrayList<Entry>();
    ArrayList<Entry> ltoa_jabo2 = new ArrayList<Entry>();

    /*
    * Set Of NE DOWN AGING Chart Data
    * */
    ArrayList<BarEntry> bar1 = new ArrayList<BarEntry>();
    ArrayList<BarEntry> bar2 = new ArrayList<BarEntry>();
    ArrayList<BarEntry> bar3 = new ArrayList<BarEntry>();
    ArrayList<BarEntry> bar4 = new ArrayList<BarEntry>();
    ArrayList<BarEntry> bar5 = new ArrayList<BarEntry>();
    BarDataSet d;
    BarDataSet d1;
    BarDataSet d3;
    BarDataSet d4;
    BarDataSet d5;
    ArrayList<IBarDataSet> dataSets;
    BarData data;

    /*
    * Set of NAV 2/3/4G Chart Data
    * */
    ArrayList<Entry> NAVJABO1_2g = new ArrayList<Entry>();
    ArrayList<Entry> NAVJABO2_2g = new ArrayList<Entry>();
    ArrayList<Entry> NAVCENTRAL_2g = new ArrayList<Entry>();
    ArrayList<Entry> NAVEAST_2g = new ArrayList<Entry>();
    ArrayList<Entry> NAVNORTH_2g = new ArrayList<Entry>();
    ArrayList<Entry> NAVSS_2g = new ArrayList<Entry>();
    ArrayList<Entry> NAVNS_2g = new ArrayList<Entry>();
    ArrayList<Entry> NAVJABO1_3g = new ArrayList<Entry>();
    ArrayList<Entry> NAVJABO2_3g = new ArrayList<Entry>();
    ArrayList<Entry> NAVCENTRAL_3g = new ArrayList<Entry>();
    ArrayList<Entry> NAVEAST_3g = new ArrayList<Entry>();
    ArrayList<Entry> NAVNORTH_3g = new ArrayList<Entry>();
    ArrayList<Entry> NAVSS_3g = new ArrayList<Entry>();
    ArrayList<Entry> NAVNS_3g = new ArrayList<Entry>();
    ArrayList<Entry> NAVJABO1_4g = new ArrayList<Entry>();
    ArrayList<Entry> NAVJABO2_4g = new ArrayList<Entry>();
    ArrayList<Entry> NAVCENTRAL_4g = new ArrayList<Entry>();
    ArrayList<Entry> NAVEAST_4g = new ArrayList<Entry>();
    ArrayList<Entry> NAVNORTH_4g = new ArrayList<Entry>();
    ArrayList<Entry> NAVSS_4g = new ArrayList<Entry>();
    ArrayList<Entry> NAVNS_4g = new ArrayList<Entry>();
    ArrayList<String> x_NAV_2g = new ArrayList<String>();
    ArrayList<String> x_NAV_3g = new ArrayList<String>();
    ArrayList<String> x_NAV_4g = new ArrayList<String>();

    SwipeRefreshLayout mySwipeRefreshLayout;

    private static LineChart lineChart;

    private String Dashboard_Type;

    private static final String ARG_DASH_TYPE = "dashboard_type";

    MyMarkerView mv;

    public DashNav2g() {
    }

    public static DashNav2g newInstance(String dashboard_Type) {
        DashNav2g dashboard = new DashNav2g();
        Bundle args = new Bundle();
        args.putString(ARG_DASH_TYPE, dashboard_Type);
        dashboard.setArguments(args);
        return dashboard;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dashboard_Type = getArguments().getString(ARG_DASH_TYPE);
        LOG_TAG = getString(R.string.app_log);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_chartnav2g, container, false);
        lineChart = (LineChart) rootView.findViewById(R.id.chart);
        lineChart.invalidate();
        mv = new MyMarkerView(getActivity().getApplicationContext(), R.layout.snapshot2g);
        mySwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshGraph();
                    }
                }
        );
        return rootView;
    }

    private void refreshGraph() {
        switch (Dashboard_Type) {
            case "LTOA":
                GetDashboardLTOA("LTOA", "daily", "oneweek");
                Log.v("Child:", "DashNav2g type: LTOA");
                break;
            case "NAV2G":
                GenerateNAV2GRegion();
                Log.v("Child:", "DashNav2g type: NAV2G");
                break;
            case "NAV3G":
                GenerateNAV3GRegion();
                Log.v("Child:", "DashNav2g type: NAV3G");
                break;
            case "NAV4G":
                GenerateNAV4GRegion();
                Log.v("Child:", "DashNav2g type: NAV4G");
                break;
            default:
                break;
        }
    }

    private void setupChart(LineChart chart, LineData data, int color) {
        chart.setOnChartGestureListener(this);
        chart.setOnChartValueSelectedListener(this);
        chart.setDrawGridBackground(false);

        // no description text
        chart.setDescription("");
        chart.setNoDataTextDescription("No Chart Data...");

        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true);

        chart.setMarkerView(mv); // Set the marker to the chart

        // add data
        chart.setData(data);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(true);
        l.setForm(Legend.LegendForm.SQUARE);

        chart.animateX(2500);
        chart.invalidate();
    }

    private void Refresh() {
        mySwipeRefreshLayout.setRefreshing(true);
    }

    private void unRefresh() {
        mySwipeRefreshLayout.setRefreshing(false);
    }

    private void ClearLTOAData() {
        x_ltoa.clear();
        ltoa_central.clear();
        ltoa_east.clear();
        ltoa_jabo1.clear();
        ltoa_jabo2.clear();
        ltoa_north.clear();
        ltoa_nw.clear();
        ltoa_sw.clear();
    }

    private void ClearNAVData() {
        NAVJABO1_2g.clear();
        NAVJABO2_2g.clear();
        NAVCENTRAL_2g.clear();
        NAVEAST_2g.clear();
        NAVNORTH_2g.clear();
        NAVSS_2g.clear();
        NAVNS_2g.clear();
        NAVJABO1_3g.clear();
        NAVJABO2_3g.clear();
        NAVCENTRAL_3g.clear();
        NAVEAST_3g.clear();
        NAVNORTH_3g.clear();
        NAVSS_3g.clear();
        NAVNS_3g.clear();
        NAVJABO1_4g.clear();
        NAVJABO2_4g.clear();
        NAVCENTRAL_4g.clear();
        NAVEAST_4g.clear();
        NAVNORTH_4g.clear();
        NAVSS_4g.clear();
        NAVNS_4g.clear();
        x_NAV_2g.clear();
        x_NAV_3g.clear();
        x_NAV_4g.clear();
    }

    private void ClearAgingData() {
        bar1.clear();
        bar2.clear();
        bar3.clear();
        bar4.clear();
        bar5.clear();
    }

    private void GenerateNAV2GRegion() {
        GetNetworkAvailability("2g", "daily", "oneday", NAVJABO1_2g, NAVJABO2_2g, NAVCENTRAL_2g, NAVEAST_2g, NAVNORTH_2g, NAVNS_2g, NAVSS_2g, x_NAV_2g);
    }

    private void GenerateNAV3GRegion() {
        GetNetworkAvailability("3g", "daily", "oneday", NAVJABO1_3g, NAVJABO2_3g, NAVCENTRAL_3g, NAVEAST_3g, NAVNORTH_3g, NAVNS_3g, NAVSS_3g, x_NAV_3g);
    }

    private void GenerateNAV4GRegion() {
        GetNetworkAvailability("4g", "daily", "oneday", NAVJABO1_4g, NAVJABO2_4g, NAVCENTRAL_4g, NAVEAST_4g, NAVNORTH_4g, NAVNS_4g, NAVSS_4g, x_NAV_4g);
    }

    public void GetNetworkAvailability(final String type, final String timespan, final String _timespan, final ArrayList<Entry> e1, final ArrayList<Entry> e2, final ArrayList<Entry> e3, final ArrayList<Entry> e4, final ArrayList<Entry> e5, final ArrayList<Entry> e6, final ArrayList<Entry> e7, final ArrayList<String> x1) {
        String url = "/dashboard/getnetworkavailability";
        final RequestParams params = new RequestParams();
        params.add("type", type);
        params.add("timespan", timespan);
        params.add("_timespan", _timespan);

        client.post(HttpUtils.getAbsoluteUrlPhp(url), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
                ClearNAVData();
                Refresh();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                JSONArray response_jabo1 = null;
                JSONArray response_jabo2 = null;
                JSONArray response_central = null;
                JSONArray response_east = null;
                JSONArray response_north = null;
                JSONArray response_ns = null;
                JSONArray response_ss = null;
                try {
                    System.out.println(response);

                    response_jabo1 = response.getJSONObject("series_data").getJSONArray("JABODETABEK 1");
                    response_jabo2 = response.getJSONObject("series_data").getJSONArray("JABODETABEK 2");
                    response_central = response.getJSONObject("series_data").getJSONArray("CENTRAL");
                    response_east = response.getJSONObject("series_data").getJSONArray("EAST");
                    response_north = response.getJSONObject("series_data").getJSONArray("NORTH");
                    response_ns = response.getJSONObject("series_data").getJSONArray("NORTHERN SUMATERA");
                    response_ss = response.getJSONObject("series_data").getJSONArray("SOUTHERN SUMATERA");

                    Date df1;
                    long dv0;
                    SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

                    try {
                        for (int i = 0; i < response_jabo1.length(); i++) {
                            JSONObject data = response_jabo1.getJSONObject(i);
                            float val = (float) data.getDouble("subs");
                            e1.add(new Entry(val, i));

                            dv0 = Long.valueOf(data.getString("time"));
                            df1 = new Date(dv0);
                            String vv1 = sdf.format(df1);
                            x1.add(vv1);
                        }

                        for (int i = 0; i < response_jabo2.length(); i++) {
                            JSONObject data = response_jabo2.getJSONObject(i);
                            float val = (float) data.getDouble("subs");
                            e2.add(new Entry(val, i));

                            if (response_jabo2.length() > response_jabo1.length()) {
                                if (i == 0) x1.clear();
                                dv0 = Long.valueOf(data.getString("time"));
                                df1 = new Date(dv0);
                                String vv1 = sdf.format(df1);
                                x1.add(vv1);
                            }
                        }

                        for (int i = 0; i < response_central.length(); i++) {
                            JSONObject data = response_central.getJSONObject(i);
                            float val = (float) data.getDouble("subs");
                            e3.add(new Entry(val, i));

                            if (response_central.length() > response_jabo2.length()) {
                                if (i == 0) x1.clear();
                                dv0 = Long.valueOf(data.getString("time"));
                                df1 = new Date(dv0);
                                String vv1 = sdf.format(df1);
                                x1.add(vv1);
                            }
                        }

                        for (int i = 0; i < response_east.length(); i++) {
                            JSONObject data = response_east.getJSONObject(i);
                            float val = (float) data.getDouble("subs");
                            e4.add(new Entry(val, i));

                            if (response_east.length() > response_central.length()) {
                                if (i == 0) x1.clear();
                                dv0 = Long.valueOf(data.getString("time"));
                                df1 = new Date(dv0);
                                String vv1 = sdf.format(df1);
                                x1.add(vv1);
                            }
                        }

                        for (int i = 0; i < response_north.length(); i++) {
                            JSONObject data = response_north.getJSONObject(i);
                            float val = (float) data.getDouble("subs");
                            e5.add(new Entry(val, i));

                            if (response_north.length() > response_east.length()) {
                                if (i == 0) x1.clear();
                                dv0 = Long.valueOf(data.getString("time"));
                                df1 = new Date(dv0);
                                String vv1 = sdf.format(df1);
                                x1.add(vv1);
                            }
                        }

                        for (int i = 0; i < response_ns.length(); i++) {
                            JSONObject data = response_ns.getJSONObject(i);
                            float val = (float) data.getDouble("subs");
                            e6.add(new Entry(val, i));

                            if (response_ns.length() > response_north.length()) {
                                if (i == 0) x1.clear();
                                dv0 = Long.valueOf(data.getString("time"));
                                df1 = new Date(dv0);
                                String vv1 = sdf.format(df1);
                                x1.add(vv1);
                            }
                        }

                        for (int i = 0; i < response_ss.length(); i++) {
                            JSONObject data = response_ss.getJSONObject(i);
                            float val = (float) data.getDouble("subs");
                            e7.add(new Entry(val, i));

                            if (response_ss.length() > response_ns.length()) {
                                if (i == 0) x1.clear();
                                dv0 = Long.valueOf(data.getString("time"));
                                df1 = new Date(dv0);
                                String vv1 = sdf.format(df1);
                                x1.add(vv1);
                            }
                        }

                        setMappingNAVData(e1, e2, e3, e4, e5, e6, e7, x1, type);
                        unRefresh();
                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Toast.makeText(getActivity().getApplicationContext(), String.format(getString(R.string.failed_get_nav_dash), type.toUpperCase().toString()), Toast.LENGTH_SHORT).show();
                        e1.add(new Entry(0f, 0));
                        e2.add(new Entry(0f, 0));
                        e3.add(new Entry(0f, 0));
                        e4.add(new Entry(0f, 0));
                        e5.add(new Entry(0f, 0));
                        e6.add(new Entry(0f, 0));
                        e7.add(new Entry(0f, 0));
                        x1.add("No Data");
                        setMappingNAVData(e1, e2, e3, e4, e5, e6, e7, x1, type);
                        unRefresh();
                    }
                } catch (JSONException e) {
//                    Toast.makeText(getActivity().getApplicationContext(), String.format(getString(R.string.failed_get_nav_dash), type.toUpperCase().toString()), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    e1.add(new Entry(0f, 0));
                    e2.add(new Entry(0f, 0));
                    e3.add(new Entry(0f, 0));
                    e4.add(new Entry(0f, 0));
                    e5.add(new Entry(0f, 0));
                    e6.add(new Entry(0f, 0));
                    e7.add(new Entry(0f, 0));
                    x1.add("No Data");
                    setMappingNAVData(e1, e2, e3, e4, e5, e6, e7, x1, type);
                    unRefresh();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(throwable);
//                Toast.makeText(getActivity().getApplicationContext(), String.format(getString(R.string.failed_get_nav_dash), type.toUpperCase().toString()), Toast.LENGTH_SHORT).show();
                unRefresh();
            }
        });
    }

    public void GetDashboardDataAging() {
        String url = "/dashboard/getdashboarddata";
        client.post(HttpUtils.getAbsoluteUrlPhp(url), new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
                Refresh();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray agingdata = response.getJSONArray("agingdata");
                    try {
                        for (int i = 0; i < agingdata.length(); i++) {
                            JSONObject data = agingdata.getJSONObject(i);
                            String region = data.getString("REGION").toString().toLowerCase();
                            bar1.add((new BarEntry(data.getInt("ZERO_TO_FOUR_H"), i)));
                            bar2.add((new BarEntry(data.getInt("FOUR_TO_TWENTYFOUR_H"), i)));
                            bar3.add((new BarEntry(data.getInt("ONE_TO_THREE_D"), i)));
                            bar4.add((new BarEntry(data.getInt("THREE_TO_SEVEN_D"), i)));
                            bar5.add((new BarEntry(data.getInt("MORE_THAN_SEVEN_D"), i)));
                        }
                        setAgingData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(throwable);
//                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.failed_get_aging_dash), Toast.LENGTH_SHORT).show();
                unRefresh();
            }
        });
    }

    public void GetDashboardLTOA(final String aging, final String type, final String timespan) {
        String url = "/dashboard/getchartdata";
        final RequestParams params = new RequestParams();

        params.add("aging", aging);
        params.add("type", type);
        params.add("timespan", timespan);

        client.post(HttpUtils.getAbsoluteUrlPhp(url), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
                ClearLTOAData();
                Refresh();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    /*
                    * Get Response from REST Api
                    * */
                    JSONArray series_data_central = response.getJSONObject("series_data").getJSONArray("CENTRAL");
                    JSONArray series_data_east = response.getJSONObject("series_data").getJSONArray("EAST");
                    JSONArray series_data_jabo1 = response.getJSONObject("series_data").getJSONArray("JABO 1");
                    JSONArray series_data_jabo2 = response.getJSONObject("series_data").getJSONArray("JABO 2");
                    JSONArray series_data_north = response.getJSONObject("series_data").getJSONArray("NORTH");
                    JSONArray series_data_nw = response.getJSONObject("series_data").getJSONArray("NORTHERN WEST");
                    JSONArray series_data_sw = response.getJSONObject("series_data").getJSONArray("SOUTHERN WEST");

                    try {
                        for (int i = 0; i < series_data_central.length(); i++) {
                            JSONObject data_central = series_data_central.getJSONObject(i);
                            JSONObject data_east = series_data_east.getJSONObject(i);
                            JSONObject data_jabo1 = series_data_jabo1.getJSONObject(i);
                            JSONObject data_jabo2 = series_data_jabo2.getJSONObject(i);
                            JSONObject data_north = series_data_north.getJSONObject(i);
                            JSONObject data_nw = series_data_nw.getJSONObject(i);
                            JSONObject data_sw = series_data_sw.getJSONObject(i);

                            long dv0 = Long.valueOf(data_central.getString("time"));
                            Date df1 = new Date(dv0);
                            SimpleDateFormat sdf = new SimpleDateFormat("d MMM");
                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                            String vv1 = sdf.format(df1);
                            x_ltoa.add(vv1);

                            float val_central = (float) data_central.getDouble("subs");
                            float val_east = (float) data_east.getDouble("subs");
                            float val_jabo1 = (float) data_jabo1.getDouble("subs");
                            float val_jabo2 = (float) data_jabo2.getDouble("subs");
                            float val_north = (float) data_north.getDouble("subs");
                            float val_nw = (float) data_nw.getDouble("subs");
                            float val_sw = (float) data_sw.getDouble("subs");

                            ltoa_central.add(new Entry(val_central, i));
                            ltoa_east.add(new Entry(val_east, i));
                            ltoa_jabo1.add(new Entry(val_jabo1, i));
                            ltoa_jabo2.add(new Entry(val_jabo2, i));
                            ltoa_north.add(new Entry(val_north, i));
                            ltoa_nw.add(new Entry(val_nw, i));
                            ltoa_sw.add(new Entry(val_sw, i));
                        }

                        setMappingLTOAData();
                        unRefresh();
                    } catch (JSONException e) {
                        setMappingLTOAData();
                        unRefresh();
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    setMappingLTOAData();
                    unRefresh();
                    e.printStackTrace();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(throwable);
//                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.failed_get_ltoa_dash), Toast.LENGTH_SHORT).show();
                unRefresh();
            }
        });
    }

    private void setAgingData() {
        d = new BarDataSet(bar1, "0H-4H");
        d.setColor(Color.GRAY);

        d1 = new BarDataSet(bar2, "4H-24H");
        d1.setColor(Color.DKGRAY);

        d3 = new BarDataSet(bar3, "1D-3D");
        d3.setColor(Color.YELLOW);

        d4 = new BarDataSet(bar4, "3D-7D");
        d4.setColor(ColorTemplate.rgb("#FFA500"));

        d5 = new BarDataSet(bar5, ">7D NE DOWN AGING");
        d5.setColor(Color.RED);

        dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(d);
        dataSets.add(d1);
        dataSets.add(d3);
        dataSets.add(d4);
        dataSets.add(d5);
    }

    private void setMappingLTOAData() {
        LineData cd = null;

        LineDataSet d1 = new LineDataSet(ltoa_central, "C ");
        d1.setColor(Color.BLUE);
        d1.setDrawValues(false);
        d1.setLineWidth(1f);
        d1.setCircleRadius(3f);
        d1.setDrawCircleHole(false);
        d1.setValueTextSize(9f);

        LineDataSet d2 = new LineDataSet(ltoa_east, "E ");
        d2.setColor(Color.YELLOW);
        d2.setDrawValues(false);
        d2.setLineWidth(1f);
        d2.setCircleRadius(3f);
        d2.setDrawCircleHole(false);
        d2.setValueTextSize(9f);

        LineDataSet d3 = new LineDataSet(ltoa_jabo1, "JB1 ");
        d3.setColor(Color.BLACK);
        d3.setDrawValues(false);
        d3.setLineWidth(1f);
        d3.setCircleRadius(3f);
        d3.setDrawCircleHole(false);
        d3.setValueTextSize(9f);

        LineDataSet d4 = new LineDataSet(ltoa_jabo2, "JB2 ");
        d4.setColor(Color.parseColor("#aa7942"));
        d4.setDrawValues(false);
        d4.setLineWidth(1f);
        d4.setCircleRadius(3f);
        d4.setDrawCircleHole(false);
        d4.setValueTextSize(9f);

        LineDataSet d5 = new LineDataSet(ltoa_north, "N ");
        d5.setColor(Color.parseColor("#FF8000"));
        d5.setDrawValues(false);
        d5.setLineWidth(1f);
        d5.setCircleRadius(3f);
        d5.setDrawCircleHole(false);
        d5.setValueTextSize(9f);

        LineDataSet d6 = new LineDataSet(ltoa_nw, "NW ");
        d6.setColor(Color.MAGENTA);
        d6.setDrawValues(false);
        d6.setLineWidth(1f);
        d6.setCircleRadius(3f);
        d6.setDrawCircleHole(false);
        d6.setValueTextSize(9f);

        LineDataSet d7 = new LineDataSet(ltoa_sw, "SW ");
        d7.setColor(Color.GREEN);
        d7.setDrawValues(false);
        d7.setLineWidth(1f);
        d7.setCircleRadius(3f);
        d7.setDrawCircleHole(false);
        d7.setValueTextSize(9f);

        ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
        sets.add(d1);
        sets.add(d2);
        sets.add(d3);
        sets.add(d4);
        sets.add(d5);
        sets.add(d6);
        sets.add(d7);

        cd = new LineData(x_ltoa, sets);
        setupChart(lineChart, cd, Color.rgb(137, 230, 81));
    }

    private void setMappingNAVData(ArrayList<Entry> e1, ArrayList<Entry> e2, ArrayList<Entry> e3, ArrayList<Entry> e4, ArrayList<Entry> e5, ArrayList<Entry> e6, ArrayList<Entry> e7, ArrayList<String> x1, String type) {
        LineData cd = null;

        LineDataSet d1 = new LineDataSet(e1, "JB1 ");
        d1.setColor(Color.BLACK);
        d1.setDrawValues(false);
        d1.setLineWidth(1f);
        d1.setCircleRadius(3f);
        d1.setDrawCircleHole(false);
        d1.setValueTextSize(9f);

        LineDataSet d2 = new LineDataSet(e2, "JB2 ");
        d2.setColor(Color.parseColor("#aa7942"));
        d2.setDrawValues(false);
        d2.setLineWidth(1f);
        d2.setCircleRadius(3f);
        d2.setDrawCircleHole(false);
        d2.setValueTextSize(9f);

        LineDataSet d3 = new LineDataSet(e3, "C ");
        d3.setColor(Color.BLUE);
        d3.setDrawValues(false);
        d3.setLineWidth(1f);
        d3.setCircleRadius(3f);
        d3.setDrawCircleHole(false);
        d3.setValueTextSize(9f);

        LineDataSet d4 = new LineDataSet(e4, "E ");
        d4.setColor(Color.YELLOW);
        d4.setDrawValues(false);
        d4.setLineWidth(1f);
        d4.setCircleRadius(3f);
        d4.setDrawCircleHole(false);
        d4.setValueTextSize(9f);

        LineDataSet d5 = new LineDataSet(e5, "N ");
        d5.setColor(Color.parseColor("#FF8000"));
        d5.setDrawValues(false);
        d5.setLineWidth(1f);
        d5.setCircleRadius(3f);
        d5.setDrawCircleHole(false);
        d5.setValueTextSize(9f);

        LineDataSet d6 = new LineDataSet(e6, "NW ");
        d6.setColor(Color.MAGENTA);
        d6.setDrawValues(false);
        d6.setLineWidth(1f);
        d6.setCircleRadius(3f);
        d6.setDrawCircleHole(false);
        d6.setValueTextSize(9f);

        LineDataSet d7 = new LineDataSet(e7, "SW ");
        d7.setColor(Color.GREEN);
        d7.setDrawValues(false);
        d7.setLineWidth(1f);
        d7.setCircleRadius(3f);
        d7.setDrawCircleHole(false);
        d7.setValueTextSize(9f);

        ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
        sets.add(d3);
        sets.add(d4);
        sets.add(d1);
        sets.add(d2);
        sets.add(d5);
        sets.add(d6);
        sets.add(d7);

        cd = new LineData(x1, sets);
        setupChart(lineChart, cd, Color.rgb(137, 230, 81));
    }

    private ArrayList<String> getRegion() {
        ArrayList<String> m = new ArrayList<String>();
        m.add("C");
        m.add("E");
        m.add("JB1");
        m.add("JB2");
        m.add("NORTH");
        m.add("NW");
        m.add("SW");
        return m;
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        refreshGraph();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        setUpActionBar();
    }

    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainNew) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(R.string.app_title);
    }
}
