package huawei.com.geol.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import huawei.com.geol.R;

/**
 * Created by M00751425 on 8/23/2016.
 */
public class DumpPage extends Fragment {

    public DumpPage() {
        // Required empty public constructor
    }

    public static DumpPage newInstance() {
        DumpPage fragment = new DumpPage();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_text, container, false);
        TextView txtView = (TextView) v.findViewById(R.id.txt_view);

        txtView.setText(
                String.format("Parent Fragment : %s", "Dump")
        );

        return v;
    }
}
