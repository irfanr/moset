package huawei.com.geol.app;

import android.Manifest;
import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import huawei.com.geol.R;
import huawei.com.geol.adapter.PopupAdapter;
import huawei.com.geol.oms.OverlappingMarkerSpiderfier;

/**
 * Created by md101 on 12/22/16.
 */

public class OnlineSearch
        extends AppCompatActivity
        implements GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerDragListener,
        SwipeRefreshLayout.OnRefreshListener {
    MapView mMapView;
    private GoogleMap googleMap;
    AsyncHttpClient client = new AsyncHttpClient();
    List<Integer> icons4g = new ArrayList<>(4);
    List<Integer> icons3g = new ArrayList<>(4);
    List<Integer> icons2g = new ArrayList<>(4);
    ProgressBar mProgressBarParent;
    List<Marker> markers = new ArrayList<Marker>();
    List<Marker> normal = new ArrayList<Marker>();
    List<Marker> alarm = new ArrayList<Marker>();
    List<Marker> down = new ArrayList<Marker>();
    List<Marker> site2G = new ArrayList<Marker>();
    List<Marker> site3G = new ArrayList<Marker>();
    List<Marker> site4G = new ArrayList<Marker>();
    Switch aSwitch, bSwitch, cSwitch, dSwitch, eSwitch, fSwitch;
    TextView normalcount, majorcount, criticalcount;

    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    RepoAdapter mRepoAdapter;
    int currentPage = 1;
    final int DEFAULT_PER_PAGE = 1000;
    int perPage = DEFAULT_PER_PAGE;
    String siteid_;
    RelativeLayout frmalarm;
    Button close1;
    boolean isLoading = false;

    public void CloseAlarm() {
        mRepoAdapter.repoList.clear();
        mRepoAdapter.notifyDataSetChanged();
        loadData(1, 1000, "XXXXXXXX");
        frmalarm.setVisibility(View.GONE);
    }

    private static class RepoAdapter extends RecyclerView.Adapter<RepoHolder> {
        LinkedHashMap<Integer, List<APIClient.Alarm>> repoMap;
        List<APIClient.Alarm> repoList;
        FragmentManager fm;

        public RepoAdapter(FragmentManager fm_) {
            repoMap = new LinkedHashMap<Integer, List<APIClient.Alarm>>();
            repoList = new ArrayList<APIClient.Alarm>();
            this.fm = fm_;
        }

        @Override
        public RepoHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            repoList.clear();

            final View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_alarm, viewGroup, false);
            final TextView evID = (TextView) view.findViewById(R.id.txtEventID);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Bundle args = new Bundle();
                    args.putString("eventid", evID.getText().toString());

                    DetailAlarm detailAlarm = new DetailAlarm();
                    detailAlarm.setArguments(args);
                    detailAlarm.show(fm, "");
                    Toast.makeText(view.getContext(), "Getting detail of: " +
                            evID.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
            return new RepoHolder(view);
        }

        @Override
        public void onBindViewHolder(RepoHolder repoHolder, int i) {
            APIClient.Alarm repo = getItem(i);
            if (repo == null) {
                return;
            }
            repoHolder.loadRepo(repo);
        }

        private APIClient.Alarm getItem(int position) {
            int listSize = 0;

            if (repoList.size() > position) {
                return repoList.get(position);
            }

            repoList = new ArrayList<APIClient.Alarm>();
            for (List<APIClient.Alarm> list : repoMap.values()) {
                repoList.addAll(list);
                listSize = listSize + list.size();
                if (listSize > position) {
                    break;
                }
            }
            if (repoList.size() > 0) {
                return repoList.get(position);
            }
            return null;
        }

        @Override
        public int getItemCount() {
            int count = 0;
            for (List<APIClient.Alarm> list : repoMap.values()) {
                count = count + list.size();
            }
            return count;
        }

        public void onNext(List<APIClient.Alarm> repos, int page) {
            if (repos == null) {
                return;
            }
            repoMap.put(page, repos);
            notifyDataSetChanged();
        }
    }

    private static class RepoHolder extends RecyclerView.ViewHolder {
        ImageView imageAvatar;
        TextView alarmname;
        TextView node;
        TextView siteid;
        TextView ttno;
        TextView occurence;
        TextView evID;

        public RepoHolder(View itemView) {
            super(itemView);
            imageAvatar = (ImageView) itemView.findViewById(R.id.imageView_avatar);
            alarmname = (TextView) itemView.findViewById(R.id.textView_alarmname);
            node = (TextView) itemView.findViewById(R.id.textView_node);
            siteid = (TextView) itemView.findViewById(R.id.textView_siteID);
            ttno = (TextView) itemView.findViewById(R.id.textView_TTNO);
            occurence = (TextView) itemView.findViewById(R.id.textView_alarmoccurrence);
            evID = (TextView) itemView.findViewById(R.id.txtEventID);
            ((ImageView) itemView.findViewById(R.id.imageView_triangle)).
                    setColorFilter(itemView
                            .getContext()
                            .getResources()
                            .getColor(R.color.blue_light));

        }

        public void loadRepo(APIClient.Alarm repo) {
            alarmname.setText(repo.alarmname);
            node.setText(repo.node);
            siteid.setText(repo.siteid);
            ttno.setText(repo.ttno);
            occurence.setText(repo.lassoccurence);
            evID.setText(String.valueOf(repo.eventid));

            int iconAvatar;

            switch (repo.severity.toString().toUpperCase()) {
                case "MAJOR":
                    iconAvatar = R.drawable.major;
                    break;
                case "MINOR":
                    iconAvatar = R.drawable.minor;
                    break;
                default:
                    iconAvatar = R.drawable.critical;
                    break;
            }

            Picasso.with(imageAvatar.getContext())
                    .load(iconAvatar)
                    .resize(200, 200)
                    .error(R.drawable.ic_github_placeholder)
                    .placeholder(R.drawable.ic_github_placeholder)
                    .centerCrop()
                    .into(imageAvatar);
        }
    }

    private void loadData(final int page, final int perPage, final String siteid_) {
        new AsyncTask<Integer, Void, List<APIClient.Alarm>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                onLoadingStarted();
            }

            @Override
            protected List<APIClient.Alarm> doInBackground(Integer... params) {
                isLoading = true;
                try {
                    return APIClient.getClient()
                            .searchAlarm(
                                    perPage,
                                    params[0],
                                    "LastOccurrence",
                                    siteid_,
                                    "",
                                    "",
                                    "",
                                    "",
                                    "desc").rows;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.Alarm> repos) {
                isLoading = false;
                if (repos != null) {
                    mRepoAdapter.onNext(repos, page);
                } else {
                }

                onLoadingFinished();

                currentPage = page;
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }.execute(page);
    }

    @Override
    public void onRefresh() {
        loadData(1, perPage, siteid_);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.onlinesearch);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        frmalarm = (RelativeLayout) findViewById(R.id.frmDetail);

        close1 = (Button) findViewById(R.id.ClickClose);
        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseAlarm();
            }
        });

        frmalarm.setVisibility(View.GONE);

        // for detail alarms
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setEnabled(false);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewAlarm);
        LinearLayoutManager manager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,
                false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mRepoAdapter = new RepoAdapter(this.getFragmentManager()));
        // end of

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setTitle("Customer Complain");

        createIcon();

        setSwitch();

//        setCount("1", "2", "3");

        mMapView = (MapView) findViewById(R.id.mapOnline);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();

        mProgressBarParent = (ProgressBar) findViewById(R.id.progressBarOnline);

        onLoadingFinished();

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(12.0f);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(1009)
                .build();

        autocompleteFragment.setFilter(typeFilter);

        autocompleteFragment.setHint("Click to find places...");

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(this.getClass().toString(), "Place: " + place.getName() + " Lat: " + place.getLatLng().latitude + " Lon: " + place.getLatLng().longitude);//get place details here
                changeCustComplain(place.getLatLng().latitude, place.getLatLng().longitude, place.getAddress().toString());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(this.getClass().toString(), "An error occurred: " + status);
            }
        });

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMapToolbarEnabled(false);

                // For dropping a marker at a point on the Map
                LatLng KoKas = new LatLng(-6.224220, 106.842242);

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(KoKas).zoom(9).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    private void changeCustComplain(double lat, double lon, String placename) {
        setAllChecked();
        loadNearbySites(lat, lon, placename);
    }

    private void reDrawLogicalMarkers(List<Marker> markers, boolean isHide) {
        for (Marker item : markers) {
            item.setVisible(isHide);
        }
    }

    private void setAllChecked() {
        aSwitch.setChecked(true);
        bSwitch.setChecked(true);
        cSwitch.setChecked(true);
        dSwitch.setChecked(true);
        eSwitch.setChecked(true);
        fSwitch.setChecked(true);
    }

    private void setSwitch() {
        normalcount = (TextView) findViewById(R.id.txtNormalCount);
        majorcount = (TextView) findViewById(R.id.txtMajorCount);
        criticalcount = (TextView) findViewById(R.id.txtCriticalCount);

        // switch normal
        aSwitch = (Switch) findViewById(R.id.switch1);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(normal, true);
                } else {
                    reDrawLogicalMarkers(normal, false);
                }
            }
        });

        // switch alarm
        bSwitch = (Switch) findViewById(R.id.switch2);
        bSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(alarm, true);
                } else {
                    reDrawLogicalMarkers(alarm, false);
                }
            }
        });

        cSwitch = (Switch) findViewById(R.id.switch3);
        cSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(down, true);
                } else {
                    reDrawLogicalMarkers(down, false);
                }
            }
        });

        dSwitch = (Switch) findViewById(R.id.switch2G);
        dSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(site2G, true);
                } else {
                    reDrawLogicalMarkers(site2G, false);
                }
            }
        });

        eSwitch = (Switch) findViewById(R.id.switch3G);
        eSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(site3G, true);
                } else {
                    reDrawLogicalMarkers(site3G, false);
                }
            }
        });

        fSwitch = (Switch) findViewById(R.id.switch4G);
        fSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reDrawLogicalMarkers(site4G, true);
                } else {
                    reDrawLogicalMarkers(site4G, false);
                }
            }
        });
    }

    public void onLoadingStarted() {
        mProgressBarParent.setVisibility(View.VISIBLE);
    }

    public void onLoadingFinished() {
        mProgressBarParent.setVisibility(View.INVISIBLE);
    }

    private void createIcon() {
        //Icons Index 0=alarm, -1=down, 2=normal
        icons2g.add(R.drawable.ic_alarm_2g_new);
        icons3g.add(R.drawable.ic_alarm_3g_new);
        icons4g.add(R.drawable.ic_alarm_4g_new);
        icons2g.add(R.drawable.ic_down_2g_new);
        icons3g.add(R.drawable.ic_down_3g_new);
        icons4g.add(R.drawable.ic_down_4g_new);
        icons2g.add(R.drawable.ic_normal_2g_new);
        icons3g.add(R.drawable.ic_normal_3g_new);
        icons4g.add(R.drawable.ic_normal_4g_new);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String snip = marker.getSnippet().toString();
        loadData(1, perPage, snip.split(",")[0].split(":")[1].trim());
        frmalarm.setVisibility(View.VISIBLE);
        Toast.makeText(this, marker.getTitle(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void setViewMarkers(LatLng lng, int zoom) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(lng).zoom(zoom).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void loadNearbySites(final double lat, final double lon, final String placename) {
        if (markers.size() > 0) {
            markers.clear();
            googleMap.clear();
            normal.clear();
            down.clear();
            alarm.clear();
        }

        new AsyncTask<Integer, Void, List<APIClient.NearbySites>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                onLoadingStarted();
            }

            @Override
            protected List<APIClient.NearbySites> doInBackground(Integer... params) {
                try {
                    return APIClient.getClient()
                            .nearbysites(1, lat, lon, "all").nearby;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.NearbySites> repos) {
                if (repos == null) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Error getting data nearby sites...",
                            Toast.LENGTH_LONG).show();
                } else {
                    Log.i(this.getClass().toString(), "Repos Data: " + repos.toString());
                }
                onLoadingFinished();
                try {
                    DrawMarkers(repos, lat, lon, placename);
                    setCount(String.valueOf(normal.size()), String.valueOf(alarm.size()), String.valueOf(down.size()));
                } catch (Exception e) {
                }
            }
        }.execute(1);
    }

    private void setCount() {
        normalcount.setText("0");
        majorcount.setText("0");
        criticalcount.setText("0");
    }

    private void DrawMarkers(List<APIClient.NearbySites> repos, double lat, double lon, String placename) {
        setCount();

        LatLng repo_latlng_point = new LatLng(lat, lon);
        Marker markerPoint = googleMap.addMarker(new MarkerOptions()
                .position(repo_latlng_point)
                .title("Center Point")
                .snippet("Address: " + placename)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.centerpoi1))
                .draggable(true)
        );

        markers.add(markerPoint);

        if (repos != null) {
            for (Iterator<APIClient.NearbySites> iter = repos.iterator(); iter.hasNext(); ) {
                APIClient.NearbySites repo = iter.next();
                LatLng repo_latlng = new LatLng(repo.latitude, repo.longitude);

                String technology = repo.nettype;

                Marker marker = googleMap.addMarker(new MarkerOptions()
                        .position(repo_latlng)
                        .title(repo.site)
                        .snippet("Site ID: " + repo.site_Id + ", Tower Provider: " + repo.tprovider)
                );

                String isD = "2";

                try {
                    isD = repo.isDead;
                    if (isD == null)
                        isD = "-1";
                    if (isD.toString().equalsIgnoreCase("null"))
                        isD = "-1";
                } catch (Exception e) {

                }

                switch (isD) {
                    case "-1":
                        if (technology.equalsIgnoreCase("2G")) {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons2g.get(2)));
                            site2G.add(marker);
                        } else if (technology.equalsIgnoreCase("3G")) {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons3g.get(2)));
                            site3G.add(marker);
                        } else {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons4g.get(2)));
                            site4G.add(marker);
                        }
                        normal.add(marker);
                        break;
                    case "0":
                        if (technology.equalsIgnoreCase("2G")) {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons2g.get(0)));
                            site2G.add(marker);
                        } else if (technology.equalsIgnoreCase("3G")) {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons3g.get(0)));
                            site3G.add(marker);
                        } else {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons4g.get(0)));
                            site4G.add(marker);
                        }
                        alarm.add(marker);
                        break;
                    default:
                        if (technology.equalsIgnoreCase("2G")) {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons2g.get(1)));
                            site2G.add(marker);
                        } else if (technology.equalsIgnoreCase("3G")) {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons3g.get(1)));
                            site3G.add(marker);
                        } else {
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons4g.get(1)));
                            site4G.add(marker);
                        }
                        down.add(marker);
                        break;
                }
            }

            if (markers.size() > 0) {
                drawLogicalMarker();
                System.out.println("down geol: " + down.size());
                System.out.println("major geol: " + alarm.size());
                System.out.println("normal geol: " + normal.size());
            }
        }
        setViewMarkers(repo_latlng_point, 16);
    }

    private void setCount(String normal, String major, String down) {
        normalcount.setText(normal);
        majorcount.setText(major);
        criticalcount.setText(down);
    }

    private void drawLogicalMarker() {
        googleMap.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater()));
        googleMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng dragPosition = marker.getPosition();
        double dragLat = dragPosition.latitude;
        double dragLong = dragPosition.longitude;
        Log.i("info", "on drag end :" + dragLat + " dragLong :" + dragLong);
        Toast.makeText(getApplicationContext(), "Marker Dragged..!", Toast.LENGTH_LONG).show();
    }
}
