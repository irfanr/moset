package huawei.com.geol.app;

import com.google.gson.annotations.SerializedName;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

import static huawei.com.geol.constant.constant.BASE_URL_JAVA;
import static huawei.com.geol.constant.constant.BASE_URL_PHP;

public class APIClient {

    private static final String API_URL = BASE_URL_PHP;
    private static final String API_JAVA_URL = BASE_URL_JAVA;

    static final String DEFAULT_SORT = "stars";
    static final String DEFAULT_ORDER = "desc";

    final static OkHttpClient okHttpClient = new OkHttpClient();

    static public class SearchAlarm {
        @SerializedName("total")
        int total;
        @SerializedName("rows")
        List<Alarm> rows;
        @SerializedName("RECEIVED_TIME")
        public String RECEIVED_TIME;
        @SerializedName("nearby")
        List<NearbySites> nearby;
    }

    static public class SearchNEDown {
        @SerializedName("nedown")
        List<NEDown> nedown;
        @SerializedName("nedowncustom")
        List<NEDown> nedown2;
        @SerializedName("user")
        public String user;
        @SerializedName("user_khusus")
        public String user_khusus;
        @SerializedName("sites")
        List<Site> sites;
        @SerializedName("alarmdetail")
        List<AlarmDetail> detail;
        @SerializedName("managers")
        List<mgr> mgrs;
        @SerializedName("supervisors")
        List<spv> spvs;
        @SerializedName("need_update")
        String need_update;
    }

    static public class spv {
        @SerializedName("id")
        public int id;
        @SerializedName("manager_area_id")
        public int manager_area_id;
        @SerializedName("supervisor_area")
        public String supervisor_area;
    }

    static public class mgr {
        @SerializedName("id")
        public int id;
        @SerializedName("gm_area")
        public int gm_area;
        @SerializedName("area_manager")
        public String area_manager;
    }

    static public class Site {
        @SerializedName("siteid")
        public String siteid;
        @SerializedName("sitename")
        public String sitename;
        @SerializedName("bsc_rnc")
        public String bsc_rnc;
        @SerializedName("latitude")
        public Double latitude;
        @SerializedName("longitude")
        public Double longitude;
        @SerializedName("tower_id")
        public int tower_id;
        @SerializedName("tower_provider")
        public String tower_provider;
        @SerializedName("technology_id")
        public int technology_id;
    }

    static public class NearbySites {
        @SerializedName("site_Id")
        public String site_Id;
        @SerializedName("POC_ID")
        public String POC_ID;
        @SerializedName("site")
        public String site;
        @SerializedName("longitude")
        public Double longitude;
        @SerializedName("latitude")
        public Double latitude;
        @SerializedName("nettype")
        public String nettype;
        @SerializedName("tprovider")
        public String tprovider;
        @SerializedName("isDead")
        public String isDead;
    }

    static public class Alarm {
        @SerializedName("EventID")
        public int eventid;
        @SerializedName("Alarmname")
        public String alarmname;
        @SerializedName("Node")
        public String node;
        @SerializedName("Summary")
        public String summary;
        @SerializedName("SiteId")
        public String siteid;
        @SerializedName("Site")
        public String site;
        @SerializedName("Zone")
        public String zone;
        @SerializedName("Severity")
        public String severity;
        @SerializedName("TTNo")
        public String ttno;
        @SerializedName("LastOccurrence")
        public String lassoccurence;
        @SerializedName("spvarea")
        public String spvarea;
        @SerializedName("mgrarea")
        public String mgarea;
        @SerializedName("fullname")
        public String fullname;
        @SerializedName("remarked_by")
        public String remarkedby;
        @SerializedName("remark_time")
        public String remarktime;
        @SerializedName("remark_category")
        public String remarkcat;
        @SerializedName("remark")
        public String remark;
        @SerializedName("time")
        public String time;
    }

    static public class AlarmDetail {
        @SerializedName("eventId")
        public int eventId;
        @SerializedName("siteId")
        public String siteId;
        @SerializedName("site")
        public String site;
        @SerializedName("zone")
        public String zone;
        @SerializedName("ttno")
        public String ttno;
        @SerializedName("alarmName")
        public String alarmName;
        @SerializedName("summary")
        public String summary;
        @SerializedName("node")
        public String node;
        @SerializedName("lastOccurrence")
        public String lastOccurrence;
    }

    static public class NEDown {
        @SerializedName("eventId")
        public int eventid;
        @SerializedName("lat")
        public double lat;
        @SerializedName("lon")
        public double lon;
        @SerializedName("group")
        public String group;
        @SerializedName("alarmName")
        public String alarmName;
        @SerializedName("node")
        public String node;
        @SerializedName("siteId")
        public String siteid;
        @SerializedName("site")
        public String site;
        @SerializedName("zone")
        public String zone;
        @SerializedName("ttno")
        public String ttno;
        @SerializedName("firstOccurrence")
        public String firstOccurrence;
        @SerializedName("lastOccurrence")
        public String lastOccurrence;
        @SerializedName("lastReceived")
        public String lastReceived;
        @SerializedName("firstReceived")
        public String firstReceived;
        @SerializedName("summary")
        public String summary;
        @SerializedName("managerArea")
        public String managerArea;
        @SerializedName("manager")
        public String manager;
        @SerializedName("gmArea")
        public String gmArea;
        @SerializedName("managerPhone")
        public String managerPhone;
        @SerializedName("towerProvider")
        public String towerProvider;
        @SerializedName("agingCategory")
        public String agingCategory;
        @SerializedName("ne")
        public String ne;
        @SerializedName("towerId")
        public String towerId;
    }

    interface mGeolAPI {
        @POST("/netcool/activealarmlist")
        SearchAlarm searchAlarm(@Query("rows") int rows,
                                @Query("page") int page,
                                @Query("sort") String sort,
                                @Query("site") String site_id,
                                @Query("last_occurrence") String last_occurrence,
                                @Query("ttno") String ttno,
                                @Query("severity") String severity,
                                @Query("zone") String zone,
                                @Query("order") String order);
        @GET("/netcool/getdataversion")
        SearchAlarm getDataVersion();
        @POST("/gis_alarm/sitestatus")
        SearchAlarm nearbysites(@Query("dis") int dis,
                                @Query("latitude") double latitude,
                                @Query("longitude") double longitude,
                                @Query("nettype") String nettype);
    }

    interface mGeolJavaAPI {
        @GET("/dashboard/nedown/json/export")
        SearchNEDown searchnedown();
        @GET("/dashboard/nedown/find")
        SearchNEDown searchnedown2(@Query("region") String region,
                                  @Query("manager") String manager,
                                  @Query("spv") String spv);
        @GET("/dashboard/nedown/json/export/{region}")
        SearchNEDown searchnedownregion(@Path("region") String region);
        @GET("/dashboard/nedown/json/export/site/{siteid}")
        SearchNEDown searchnedownsiteid(@Path("siteid") String siteid);
        @GET("/auth/json/imei/{data}")
        SearchNEDown authimei(@Path("data") String data);
        @GET("/auth/json/imeikhusus/{data}")
        SearchNEDown authimeikhusus(@Path("data") String data);
        @GET("/site/{siteid}")
        SearchNEDown getSite(@Path("siteid") String siteid);
        @GET("/alarm/detail/{eventid}")
        SearchNEDown getAlarmDetail(@Path("eventid") String eventid);
        @GET("/spvlall")
        SearchNEDown getAllSPV();
        @GET("/mgrlall")
        SearchNEDown getAllMGR();
        @GET("/appversion/check/{version}")
        SearchNEDown getVersion(@Path("version") String version);
    }

    public static mGeolAPI getClient() {
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setClient(new OkClient(okHttpClient))
                .build();

        return restAdapter.create(mGeolAPI.class);
    }

    public static mGeolJavaAPI getJavaClient() {
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_JAVA_URL)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setClient(new OkClient(okHttpClient))
                .build();

        return restAdapter.create(mGeolJavaAPI.class);
    }
}
