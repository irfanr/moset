package huawei.com.geol.app;

import android.Manifest;
import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import huawei.com.geol.R;
import huawei.com.geol.adapter.ExpandList;
import huawei.com.geol.adapter.Group;
import huawei.com.geol.adapter.PopupAdapter;
import huawei.com.geol.utils.HttpUtils;
import huawei.com.geol.utils.ShareUtils;

/**
 * Created by md101 on 10/18/16.
 */

public class VipSite extends AppCompatActivity implements GoogleMap.OnInfoWindowClickListener,
        SwipeRefreshLayout.OnRefreshListener {
    MapView mMapView;
    private GoogleMap googleMap;
    AsyncHttpClient client = new AsyncHttpClient();
    SparseArray<Group> groups = new SparseArray<Group>();
    List<Integer> icons4g = new ArrayList<>(4);
    List<Integer> icons3g = new ArrayList<>(4);
    List<Integer> icons2g = new ArrayList<>(4);
    List<Marker> markers = new ArrayList<Marker>();
    List<Marker> normal = new ArrayList<Marker>();
    List<Marker> alarm = new ArrayList<Marker>();
    List<Marker> down = new ArrayList<Marker>();
    ProgressBar mProgressBarParent;
    Spinner spin;
    static ArrayList<String> mgrs = new ArrayList<>();
    TextView txt1;
    ToggleButton toogle;
    Switch aSwitch, bSwitch, cSwitch;
    boolean isLoading = false;
    String siteid_, region;
    RelativeLayout frmalarm;
    Button close1;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    RepoAdapter mRepoAdapter;
    int currentPage = 1;
    final int DEFAULT_PER_PAGE = 25;
    int perPage = DEFAULT_PER_PAGE;

    public void CloseAlarm() {
        mRepoAdapter.repoList.clear();
        mRepoAdapter.notifyDataSetChanged();
        loadData(1, 1000, "XXXXXXXX");
        frmalarm.setVisibility(View.GONE);
    }

    private static class RepoAdapter extends RecyclerView.Adapter<RepoHolder> {
        LinkedHashMap<Integer, List<APIClient.Alarm>> repoMap;
        List<APIClient.Alarm> repoList;
        FragmentManager fm;

        public RepoAdapter(FragmentManager fm_) {
            repoMap = new LinkedHashMap<Integer, List<APIClient.Alarm>>();
            repoList = new ArrayList<APIClient.Alarm>();
            this.fm = fm_;
        }

        @Override
        public RepoHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            final View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_alarm, viewGroup, false);
            final TextView evID = (TextView) view.findViewById(R.id.txtEventID);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Bundle args = new Bundle();
                    args.putString("eventid", evID.getText().toString());

                    DetailAlarm detailAlarm = new DetailAlarm();
                    detailAlarm.setArguments(args);
                    detailAlarm.show(fm, "");
                    Toast.makeText(view.getContext(), "Getting detail of: " +
                            evID.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
            return new RepoHolder(view);
        }

        @Override
        public void onBindViewHolder(RepoHolder repoHolder, int i) {
            APIClient.Alarm repo = getItem(i);
            if (repo == null) {
                return;
            }
            repoHolder.loadRepo(repo);
        }

        private APIClient.Alarm getItem(int position) {
            int listSize = 0;

            if (repoList.size() > position) {
                return repoList.get(position);
            }

            repoList = new ArrayList<APIClient.Alarm>();
            for (List<APIClient.Alarm> list : repoMap.values()) {
                repoList.addAll(list);
                listSize = listSize + list.size();
                if (listSize > position) {
                    break;
                }
            }
            if (repoList.size() > 0) {
                return repoList.get(position);
            }
            return null;
        }

        @Override
        public int getItemCount() {
            int count = 0;
            for (List<APIClient.Alarm> list : repoMap.values()) {
                count = count + list.size();
            }
            return count;
        }

        public void onNext(List<APIClient.Alarm> repos, int page) {
            if (repos == null) {
                return;
            }
            repoMap.put(page, repos);
            notifyDataSetChanged();
        }
    }

    private static class RepoHolder extends RecyclerView.ViewHolder {

        ImageView imageAvatar;
        TextView alarmname;
        TextView node;
        TextView siteid;
        TextView ttno;
        TextView occurence;
        TextView evID;

        public RepoHolder(View itemView) {
            super(itemView);
            imageAvatar = (ImageView) itemView.findViewById(R.id.imageView_avatar);
            alarmname = (TextView) itemView.findViewById(R.id.textView_alarmname);
            node = (TextView) itemView.findViewById(R.id.textView_node);
            siteid = (TextView) itemView.findViewById(R.id.textView_siteID);
            ttno = (TextView) itemView.findViewById(R.id.textView_TTNO);
            occurence = (TextView) itemView.findViewById(R.id.textView_alarmoccurrence);
            evID = (TextView) itemView.findViewById(R.id.txtEventID);
            ((ImageView) itemView.findViewById(R.id.imageView_triangle)).
                    setColorFilter(itemView
                            .getContext()
                            .getResources()
                            .getColor(R.color.blue_light));

        }

        public void loadRepo(APIClient.Alarm repo) {
            alarmname.setText(repo.alarmname);
            node.setText(repo.node);
            siteid.setText(repo.siteid);
            ttno.setText(repo.ttno);
            occurence.setText(repo.lassoccurence);
            evID.setText(String.valueOf(repo.eventid));

            int iconAvatar;

            switch (repo.severity.toString().toUpperCase()) {
                case "MAJOR":
                    iconAvatar = R.drawable.major;
                    break;
                case "MINOR":
                    iconAvatar = R.drawable.minor;
                    break;
                default:
                    iconAvatar = R.drawable.critical;
                    break;
            }

            Picasso.with(imageAvatar.getContext())
                    .load(iconAvatar)
                    .resize(200, 200)
                    .error(R.drawable.ic_github_placeholder)
                    .placeholder(R.drawable.ic_github_placeholder)
                    .centerCrop()
                    .into(imageAvatar);
        }
    }

    private void loadData(final int page, final int perPage, final String siteid_) {
        new AsyncTask<Integer, Void, List<APIClient.Alarm>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                onLoadingStarted();
            }

            @Override
            protected List<APIClient.Alarm> doInBackground(Integer... params) {
                isLoading = true;
                try {
                    return APIClient.getClient()
                            .searchAlarm(
                                    perPage,
                                    params[0],
                                    "LastOccurrence",
                                    siteid_,
                                    "",
                                    "",
                                    "",
                                    region,
                                    "desc").rows;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.Alarm> repos) {
                isLoading = false;
                if (repos != null) {
                    mRepoAdapter.onNext(repos, page);
                } else {
                }

                onLoadingFinished();

                currentPage = page;
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }.execute(page);
    }

    @Override
    public void onRefresh() {
        loadData(1, perPage, siteid_);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vipsite);
        frmalarm = (RelativeLayout) findViewById(R.id.frmDetail);

        close1 = (Button) findViewById(R.id.ClickClose);
        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseAlarm();
            }
        });

        frmalarm.setVisibility(View.GONE);

        // for detail alarms
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setEnabled(false);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewAlarm);
        LinearLayoutManager manager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,
                false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mRepoAdapter = new RepoAdapter(this.getFragmentManager()));
        // end of

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        // switch normal
        aSwitch = (Switch) findViewById(R.id.switch1);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    reDrawLogicalMarkers(normal, true);
                } else {
                    // The toggle is disabled
                    reDrawLogicalMarkers(normal, false);
                }
            }
        });

        // switch alarm
        bSwitch = (Switch) findViewById(R.id.switch2);
        bSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    reDrawLogicalMarkers(alarm, true);
                } else {
                    // The toggle is disabled
                    reDrawLogicalMarkers(alarm, false);
                }
            }
        });

        // switch down
        cSwitch = (Switch) findViewById(R.id.switch3);
        cSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    reDrawLogicalMarkers(down, true);
                } else {
                    // The toggle is disabled
                    reDrawLogicalMarkers(down, false);
                }
            }
        });

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setTitle("VIP Route");
        spin = (Spinner) findViewById(R.id.spin_viproute);

        GetVipSiteList();
        createIcon();

        mMapView = (MapView) findViewById(R.id.mapViewVipSite);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();

        mProgressBarParent = (ProgressBar) findViewById(R.id.progressBarNeDownGIS);
        txt1 = (TextView) findViewById(R.id.txtSet_VIPRoute);

        onLoadingFinished();

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMapToolbarEnabled(false);

                // For dropping a marker at a point on the Map
                LatLng KoKas = new LatLng(-6.224220, 106.842242);

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(KoKas).zoom(9).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//                    @Override
//                    public void onInfoWindowClick(Marker marker) {
//                        loadData(1, perPage, marker.getTitle().toString());
//                        frmalarm.setVisibility(View.VISIBLE);
//                    }
//                });
            }
        });
    }

    private void setAllChecked() {
        aSwitch.setChecked(true);
        bSwitch.setChecked(true);
        cSwitch.setChecked(true);
    }

    private void reDrawLogicalMarkers(List<Marker> markers, boolean isHide) {
//        googleMap.clear();
        for (Marker item : markers) {
            item.setVisible(isHide);
        }
    }

    public void onLoadingStarted() {
        mProgressBarParent.setVisibility(View.VISIBLE);
    }

    public void onLoadingFinished() {
        mProgressBarParent.setVisibility(View.INVISIBLE);
    }

    public static void getVIPSite(AsyncHttpClient client, final Group group) {
        String url = "/map/getviproute";
        client.get(HttpUtils.getAbsoluteUrlPhp(url), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        group.children.add(data.getString("routename").toString());
                        System.out.println("Data ke: " + i + " " + data.getString("routename").toString());
                        mgrs.add(data.getString("routename").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
            }
        });
    }

    public void setSpin() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                this.getApplicationContext(),
                R.layout.layout_remarks,
                mgrs);
        dataAdapter.setDropDownViewResource(R.layout.listremarkcats);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parent.getContext(), "The planet is " +
                        parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spin.setAdapter(dataAdapter);
    }

    public void GetVipSiteList() {
        Group group = new Group("Please select VIP Route");
        getVIPSite(client, group);
        setSpin();
        groups.append(0, group);

        final ExpandableListView listView = (ExpandableListView) findViewById(R.id.vipsitelist);

        listView.setDividerHeight(5);
        listView.setClickable(true);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            View _lastColored;

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                final String children = (String) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);
                Toast.makeText(VipSite.this, "Getting route for: " + children, Toast.LENGTH_SHORT).show();
                int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
                parent.setItemChecked(index, true);

                parent.collapseGroup(0);
                txt1.setText(children);
                getVIPSiteStatus(children);
                setAllChecked();
                return true;
            }
        });

        ExpandList adapter = new ExpandList(this,
                groups);
        listView.setAdapter(adapter);
    }

    private void createIcon() {
        //Icons Index 0=alarm, -1=down, 2=normal
        icons2g.add(R.drawable.ic_alarm_2g_new);
        icons3g.add(R.drawable.ic_alarm_3g_new);
        icons4g.add(R.drawable.ic_alarm_4g_new);
        icons2g.add(R.drawable.ic_down_2g_new);
        icons3g.add(R.drawable.ic_down_3g_new);
        icons4g.add(R.drawable.ic_down_4g_new);
        icons2g.add(R.drawable.ic_normal_2g_new);
        icons3g.add(R.drawable.ic_normal_3g_new);
        icons4g.add(R.drawable.ic_normal_4g_new);
    }

    public void getVIPSiteStatus(String route) {
        String url = "/gis_alarm/vipsitestatus_new";
        RequestParams params = new RequestParams();
        params.add("route", route);

        googleMap.clear();
        markers.clear();

        client.post(HttpUtils.getAbsoluteUrlPhp(url), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
                onLoadingStarted();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                onLoadingFinished();
                JSONArray response_ = null;
                try {
                    System.out.println("Response: " + response);
                    response_ = response.getJSONArray("sites");
                    try {
                        LatLng repo_latlng;
                        for (int i = 0; i < response_.length(); i++) {
                            JSONObject data = response_.getJSONObject(i);
                            if (ShareUtils.isNumeric(data.getString("LATITUDE")) &&
                                    ShareUtils.isNumeric(data.getString("LONGITUDE"))) {

                                repo_latlng = new LatLng(data.getDouble("LATITUDE"), data.getDouble("LONGITUDE"));

                                StringBuilder str = new StringBuilder();
                                str.append("Site ID: " + data.getString("SITEID"));
                                str.append(System.getProperty("line.separator"));
                                str.append("Tower provider: " + data.getString("TOWER_PROVIDER"));
                                str.append(System.getProperty("line.separator"));
                                str.append("Sitename: " + data.getString("SITENAME"));
                                str.append(System.getProperty("line.separator"));
                                str.append("Sitetype: " + data.getString("SITE_TYPE"));

                                Marker marker = googleMap.addMarker(new MarkerOptions()
                                        .position(repo_latlng)
                                        .title(data.getString("SITEID"))
                                        .snippet(str.toString())
                                );

                                String technology = data.getString("TECHNOLOGY");
                                switch (data.getString("ISDEAD")) {
                                    case "-1":
                                        if (technology.equalsIgnoreCase("2G")) {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons2g.get(2)));
                                        } else if (technology.equalsIgnoreCase("3G")) {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons3g.get(2)));
                                        } else {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons4g.get(2)));
                                        }
                                        normal.add(marker);
                                        break;
                                    case "0":
                                        if (technology.equalsIgnoreCase("2G")) {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons2g.get(0)));
                                        } else if (technology.equalsIgnoreCase("3G")) {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons3g.get(0)));
                                        } else {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons4g.get(0)));
                                        }
                                        alarm.add(marker);
                                        break;
                                    default:
                                        if (technology.equalsIgnoreCase("2G")) {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons2g.get(1)));
                                        } else if (technology.equalsIgnoreCase("3G")) {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons3g.get(1)));
                                        } else {
                                            marker.setIcon(BitmapDescriptorFactory.fromResource(icons4g.get(1)));
                                        }
                                        down.add(marker);
                                        break;
                                }
                                markers.add(marker);
                            }
                        }
                        if (markers.size() > 0) {
                            drawLogicalMarker();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(throwable);
                onLoadingFinished();
            }
        });
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        loadData(1, perPage, marker.getTitle().toString());
        frmalarm.setVisibility(View.VISIBLE);
    }

    private void drawLogicalMarker() {
        googleMap.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater()));
        googleMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
