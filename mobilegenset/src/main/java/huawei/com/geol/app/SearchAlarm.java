package huawei.com.geol.app;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import huawei.com.geol.R;

import static huawei.com.geol.utils.ShareUtils.AlertMsg;

/**
 * Created by md101 on 9/3/16.
 */

public class SearchAlarm extends DialogFragment {
    Button mButton, dButton;
    EditText mEditText;
    onSubmitListener mListener;
    String text = "";
    Dialog dialog = null;
    private EditText Output;
    private int year;
    private int month;
    private int day;
    static final int DATE_PICKER_ID = 1111;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    private InputMethodManager im;

    interface onSubmitListener {
        void setOnSubmitListener(String sev, String siteid, String ttno, String lastoccur, String region);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.search_alarm);
        dialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
        mButton = (Button) dialog.findViewById(R.id.button1);
        dButton = (Button) dialog.findViewById(R.id.button2);
        Output = (EditText) dialog.findViewById(R.id.txt_date);
        Output.setInputType(InputType.TYPE_NULL);
        dButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String spinregion = "";

                try {
                    Spinner spinnerregion = (Spinner) dialog.findViewById(R.id.spin_alarmregion);
                    spinregion = spinnerregion.getSelectedItem().toString();
                    if (spinregion.toLowerCase().equalsIgnoreCase("all")) {
                        spinregion = "";
                        AlertMsg("Information", "Please select Region", v.getContext());
                    } else {
                        Uri uri = Uri.parse("http://116.66.203.240:8888/geolws/query?n=activealarm&param1=" + spinregion);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                }
            }
        });
        mButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String spindata = "";
                String spinregion = "";
                try {
                    Spinner spinner = (Spinner) dialog.findViewById(R.id.spin_severity);
                    spindata = spinner.getSelectedItem().toString();
                    if (spindata.toLowerCase().equalsIgnoreCase("all")) {
                        spindata = "";
                    }
                } catch (Exception e) {
                }
                try {
                    Spinner spinnerregion = (Spinner) dialog.findViewById(R.id.spin_alarmregion);
                    spinregion = spinnerregion.getSelectedItem().toString();
                    if (spinregion.toLowerCase().equalsIgnoreCase("all")) {
                        spinregion = "";
                    }
                } catch (Exception e) {
                }
                EditText siteid = (EditText) dialog.findViewById(R.id.txt_SiteID);
                EditText ttno = (EditText) dialog.findViewById(R.id.txt_ttno);

                String siteID = "";
                String TTNO = "";
                String date = "";

                if (!siteid.getText().toString().isEmpty())
                    siteID = siteid.getText().toString();

                if (!ttno.getText().toString().isEmpty())
                    TTNO = ttno.getText().toString();

                if (!Output.getText().toString().isEmpty())
                    date = Output.getText().toString();

                mListener.setOnSubmitListener(
                        spindata,
                        siteID,
                        TTNO,
                        date,
                        spinregion);
                dismiss();
            }
        });

        InitSpin();
        PopUpDate();
        return dialog;
    }

    private void PopUpDate() {
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        Output.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; // In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        Output.setText(sdf.format(myCalendar.getTime()));
    }

    private void InitSpin() {
        Spinner spinner = (Spinner) dialog.findViewById(R.id.spin_severity);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.severity, R.layout.spinner_alarm);
        adapter.setDropDownViewResource(R.layout.spinner_alarm);
        spinner.setAdapter(adapter);

        Spinner spinner_region = (Spinner) dialog.findViewById(R.id.spin_alarmregion);
        ArrayAdapter<CharSequence> adapter_region = ArrayAdapter.createFromResource(getActivity(),
                R.array.region, R.layout.spinner_alarm);
        adapter_region.setDropDownViewResource(R.layout.spinner_alarm);
        spinner_region.setAdapter(adapter_region);
    }
}