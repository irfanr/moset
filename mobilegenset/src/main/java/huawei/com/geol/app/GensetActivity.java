package huawei.com.geol.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import huawei.com.geol.R;
import huawei.com.geol.mqtt.Constants;
import huawei.com.geol.mqtt.MqttMessageService;
import huawei.com.geol.mqtt.PahoMqttClient;

import static huawei.com.geol.utils.ShareUtils.AlertMsg;

public class GensetActivity extends AppCompatActivity {

    private MqttAndroidClient client;
    private PahoMqttClient pahoMqttClient;
    private String TAG = "RemotePanel";
    private TextView textView3;
    private Button startengine, gensetauto, gensetman, gensetstop, transgenset, transmains, do1on, do1off, do2on, do2off;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genset);

        pahoMqttClient = new PahoMqttClient();

        client = pahoMqttClient.getMqttClient(getApplicationContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);

        textView3 = (TextView) findViewById(R.id.textView3);
        textView3.setMovementMethod(new ScrollingMovementMethod());
//        textView3.setMovementMethod(new ScrollingMovementMethod());
        gensetauto = (Button) findViewById(R.id.autoButton);
        gensetman = (Button) findViewById(R.id.manualButton);
        gensetstop = (Button) findViewById(R.id.stopButton);
//        transgenset = (Button) findViewById(R.id.transgenset);
//        transmains = (Button) findViewById(R.id.transmains);
        startengine = (Button) findViewById(R.id.startButton);

        gensetauto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView3.append(new Date()+": Auto Starting Genset.....\n");

                //String msg = textMessage.getText().toString().trim();
//                AlertMsg("information", "Auto Starting Genset.....",GensetActivity.this);
//                String msg = "{ \"id\":\"test\", \"command\": \"<30\", \"aux\":\"Texas\"}";
//                if (!msg.isEmpty()) {
//                    try {
//                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
//                    } catch (MqttException e) {
//                        e.printStackTrace();
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage());
//                    }
//                }
            }
        });

        gensetman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView3.append(new Date()+": Manual Starting Genset.....\n");

                //String msg = textMessage.getText().toString().trim();
//                AlertMsg("information", "Manual Starting Genset.....",GensetActivity.this);
//                String msg = "{ \"id\":\"test\", \"command\": \"=30\", \"aux\":\"Texas\"}";
//                if (!msg.isEmpty()) {
//                    try {
//                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
//                    } catch (MqttException e) {
//                        e.printStackTrace();
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        Log.e(TAG, e.getMessage());
//                    }
//                }
            }
        });

        gensetstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView3.append(new Date()+": Stopping Genset.....\n");

                //String msg = textMessage.getText().toString().trim();
//                AlertMsg("information", "Stopping Genset.....",GensetActivity.this);
//                String msg = "{ \"id\":\"test\", \"command\": \">30\", \"aux\":\"Texas\"}";
//                if (!msg.isEmpty()) {
//                    try {
//                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
//                    } catch (MqttException e) {
//                        e.printStackTrace();
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage());
//                    }
//                }
            }
        });

        startengine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView3.append(new Date()+": Starting Genset.....\n");

                //String msg = textMessage.getText().toString().trim();
//                AlertMsg("information", "Starting Genset.....",GensetActivity.this);
//                String msg = "{ \"id\":\"test\", \"command\": \"<33\", \"aux\":\"Texas\"}";
//                if (!msg.isEmpty()) {
//                    try {
//                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
//                    } catch (MqttException e) {
//                        e.printStackTrace();
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage());
//                    }
//                }
            }
        });

        Intent intent = new Intent(GensetActivity.this, MqttMessageService.class);
        startService(intent);

    }
}
