package huawei.com.geol.app;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.ArrayList;
import java.util.Arrays;

import huawei.com.geol.R;
import huawei.com.geol.adapter.ChildAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardChild extends Fragment {

    public DashboardChild() {
        // Required empty public constructor
    }

    public static DashboardChild newInstance() {
        return new DashboardChild();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.fragment_view_pager, container, false);
        String url = "http://wamti.org/monitoring/";
        View v = inflater.inflate(R.layout.mgenset_dash, container, false);
        WebView myWebView = (WebView) v.findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl(url);
        myWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
//        ViewPager vp = (ViewPager) v.findViewById(R.id.view_pager);
//        TabLayout tl = (TabLayout) v.findViewById(R.id.tab_layout);

//        ChildAdapter adapter = new ChildAdapter(getFragmentManager(), new ArrayList<>(Arrays.asList("LTOA", "NAV 2G", "NAV 3G", "NAV 4G")));

//        vp.setAdapter(adapter);
//        tl.setupWithViewPager(vp);

        return v;
    }

}
