package huawei.com.geol.app;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import huawei.com.geol.R;

public class AlarmFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    interface OnLoadingListener {
        void onLoadingStarted();

        void onLoadingFinished();
    }

    private String query = "android";
    private String language = "java";
    private String queryString = "%s+language:%s";
    final int DEFAULT_PER_PAGE = 10;

    private BaseAttacher mBaseAttacher;
    int currentPage = 1;
    int perPage = DEFAULT_PER_PAGE;
    boolean isLoading = false;

    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    RepoAdapter mRepoAdapter;
    OnLoadingListener mListener;

    ProgressBar mProgressBarParent;

    TextView txtFilter,
            txtFilterDataLastOccur,
            txtFilterLastOccur,
            txtFilterDataTTNo,
            txtFilterTTNo,
            txtFilterDataSite,
            txtFilterSite,
            txtFilterDataRegion,
            txtFilterRegion,
            txtFilterDataSeverity,
            txtFilterSeverity;

    Button btnShowHideFilter;

    String sev, ttno, lastoccur, siteid, region;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnLoadingListener) {
            mListener = (OnLoadingListener) activity;
        }
        mProgressBarParent = (ProgressBar) getActivity().findViewById(R.id.progressBarAlarm);
    }

    public void onLoadingStarted() {
        mProgressBarParent.setVisibility(View.VISIBLE);
    }

    public void onLoadingFinished() {
        mProgressBarParent.setVisibility(View.INVISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mProgressBarParent = (ProgressBar) getActivity().findViewById(R.id.progressBarAlarm);
        View rootView = inflater.inflate(R.layout.fragment_alarm, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setEnabled(false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewAlarm);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL,
                false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mRepoAdapter = new RepoAdapter(getActivity().getFragmentManager()));

        perPage = getPerPage(rootView.getContext());

        txtFilter = (TextView) rootView.findViewById(R.id.txtFilter);
        txtFilterDataLastOccur = (TextView) rootView.findViewById(R.id.txtFilterDataLastOccur);
        txtFilterLastOccur = (TextView) rootView.findViewById(R.id.txtFilterLastOccur);
        txtFilterDataTTNo = (TextView) rootView.findViewById(R.id.txtFilterDataTTNo);
        txtFilterTTNo = (TextView) rootView.findViewById(R.id.txtFilterTTNo);
        txtFilterDataSite = (TextView) rootView.findViewById(R.id.txtFilterDataSite);
        txtFilterSite = (TextView) rootView.findViewById(R.id.txtFilterSite);
        txtFilterSeverity = (TextView) rootView.findViewById(R.id.txtFilterSeverity);
        txtFilterDataSeverity = (TextView) rootView.findViewById(R.id.txtFilterDataSeverity);
        txtFilterRegion = (TextView) rootView.findViewById(R.id.txtFilterRegion);
        txtFilterDataRegion = (TextView) rootView.findViewById(R.id.txtFilterDataRegion);

        btnShowHideFilter = (Button) rootView.findViewById(R.id.btnShowHideFilter);

        btnShowHideFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowFilter();
            }
        });

        loadData(currentPage, perPage);

        if(getArguments() != null) {
            Bundle args = getArguments();
            sev = args.getString("sev");
            ttno = args.getString("ttno");
            lastoccur = args.getString("lastoccur");
            siteid = args.getString("siteid");
            region = args.getString("region");
        } else {
            sev = "";
            ttno = "";
            lastoccur = "";
            siteid = "";
            region = "";
        }

        setFilter(txtFilterDataLastOccur, lastoccur);
        setFilter(txtFilterDataRegion, region);
        setFilter(txtFilterDataSeverity, sev);
        setFilter(txtFilterDataTTNo, ttno);
        setFilter(txtFilterDataSite, siteid);

        hideAll();

//        onLoadingFinished();

        return rootView;
    }

    private void setFilter(TextView v, String txt) {
        v.setText(txt);
    }

    private void ShowTextView(TextView v, int i) {
        if (i == 1) {
            v.setVisibility(View.GONE);
        } else {
            v.setVisibility(View.VISIBLE);
        }
    }

    private void hideAll() {
        ShowTextView(txtFilterDataLastOccur, 1);
        ShowTextView(txtFilterLastOccur, 1);
        ShowTextView(txtFilterDataTTNo, 1);
        ShowTextView(txtFilterTTNo, 1);
        ShowTextView(txtFilterDataSite, 1);
        ShowTextView(txtFilterSite, 1);
        ShowTextView(txtFilterSeverity, 1);
        ShowTextView(txtFilterDataSeverity, 1);
        ShowTextView(txtFilterRegion, 1);
        ShowTextView(txtFilterDataRegion, 1);
    }

    public void ShowFilter() {
        if (btnShowHideFilter.getText().toString().equalsIgnoreCase("show")) {
            ShowTextView(txtFilterDataLastOccur, 0);
            ShowTextView(txtFilterLastOccur, 0);
            ShowTextView(txtFilterDataTTNo, 0);
            ShowTextView(txtFilterTTNo, 0);
            ShowTextView(txtFilterDataSite, 0);
            ShowTextView(txtFilterSite, 0);
            ShowTextView(txtFilterSeverity, 0);
            ShowTextView(txtFilterDataSeverity, 0);
            ShowTextView(txtFilterRegion, 0);
            ShowTextView(txtFilterDataRegion, 0);
            btnShowHideFilter.setText("Hide");
        } else {
            hideAll();
            btnShowHideFilter.setText("Show");
        }
    }

    /**
     * Get items to load per page onScroll.
     *
     * @param context {@link Context}
     * @return int of num of items that can be loaded onto the screen with scroll enabled
     */
    private int getPerPage(Context context) {
        //fixed item size in recyclerview. Adding 3 enables recyclerview scrolling.
        return (context.getResources().getDisplayMetrics().heightPixels
                / context.getResources().getDimensionPixelSize(R.dimen.repo_item_height)) + 3;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBaseAttacher = Mugen.with(mRecyclerView, new MugenCallbacks() {
            @Override
            public void onLoadMore() {
                loadData(currentPage + 1, perPage);
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return false;
            }
        }).start();

    }

    private void loadData(final int page, final int perPage) {
        new AsyncTask<Integer, Void, List<APIClient.Alarm>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                onLoadingStarted();
            }

            @Override
            protected List<APIClient.Alarm> doInBackground(Integer... params) {
                String q = String.format(Locale.ENGLISH,
                        queryString,
                        query,
                        language);
                isLoading = true;
                try {
                    return APIClient.getClient()
                            .searchAlarm(
                                    perPage,
                                    params[0],
                                    "LastOccurrence",
                                    siteid,
                                    lastoccur,
                                    ttno,
                                    sev,
                                    region,
                                    "desc").rows;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.Alarm> repos) {
                isLoading = false;
                if (repos != null) {
                    mRepoAdapter.onNext(repos, page);
                } else {
                }

                onLoadingFinished();

                currentPage = page;
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }.execute(page);
    }

    @Override
    public void onRefresh() {
        loadData(1, perPage);
    }

    private static class RepoAdapter extends RecyclerView.Adapter<RepoHolder> {
        LinkedHashMap<Integer, List<APIClient.Alarm>> repoMap;
        List<APIClient.Alarm> repoList;
        FragmentManager fm;

        public RepoAdapter(FragmentManager fm_) {
            repoMap = new LinkedHashMap<Integer, List<APIClient.Alarm>>();
            repoList = new ArrayList<APIClient.Alarm>();
            this.fm = fm_;
        }

        @Override
        public RepoHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            final View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_alarm, viewGroup, false);
            final TextView evID = (TextView) view.findViewById(R.id.txtEventID);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Bundle args = new Bundle();
                    args.putString("eventid", evID.getText().toString());

                    DetailAlarm detailAlarm = new DetailAlarm();
                    detailAlarm.setArguments(args);
                    detailAlarm.show(fm, "");
                    Toast.makeText(view.getContext(), "Getting detail of: " +
                            evID.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
            return new RepoHolder(view);
        }

        @Override
        public void onBindViewHolder(RepoHolder repoHolder, int i) {
            APIClient.Alarm repo = getItem(i);
            if (repo == null) {
                return;
            }
            repoHolder.loadRepo(repo);
        }

        private APIClient.Alarm getItem(int position) {
            int listSize = 0;

            if (repoList.size() > position) {
                return repoList.get(position);
            }

            repoList = new ArrayList<APIClient.Alarm>();
            for (List<APIClient.Alarm> list : repoMap.values()) {
                repoList.addAll(list);
                listSize = listSize + list.size();
                if (listSize > position) {
                    break;
                }
            }
            if (repoList.size() > 0) {
                return repoList.get(position);
            }
            return null;
        }

        @Override
        public int getItemCount() {
            int count = 0;
            for (List<APIClient.Alarm> list : repoMap.values()) {
                count = count + list.size();
            }
            return count;
        }

        public void onNext(List<APIClient.Alarm> repos, int page) {
            if (repos == null) {
                return;
            }
            repoMap.put(page, repos);
            notifyDataSetChanged();
        }
    }

    private static class RepoHolder extends RecyclerView.ViewHolder {

        ImageView imageAvatar;
        TextView alarmname;
        TextView node;
        TextView siteid;
        TextView ttno;
        TextView occurence;
        TextView evID;

        public RepoHolder(View itemView) {
            super(itemView);
            imageAvatar = (ImageView) itemView.findViewById(R.id.imageView_avatar);
            alarmname = (TextView) itemView.findViewById(R.id.textView_alarmname);
            node = (TextView) itemView.findViewById(R.id.textView_node);
            siteid = (TextView) itemView.findViewById(R.id.textView_siteID);
            ttno = (TextView) itemView.findViewById(R.id.textView_TTNO);
            occurence = (TextView) itemView.findViewById(R.id.textView_alarmoccurrence);
            evID = (TextView) itemView.findViewById(R.id.txtEventID);
            ((ImageView) itemView.findViewById(R.id.imageView_triangle)).
                    setColorFilter(itemView
                            .getContext()
                            .getResources()
                            .getColor(R.color.blue_light));

        }

        public void loadRepo(APIClient.Alarm repo) {
            alarmname.setText(repo.alarmname);
            node.setText(repo.node);
            siteid.setText(repo.siteid);
            ttno.setText(repo.ttno);
            occurence.setText(repo.lassoccurence);
            evID.setText(String.valueOf(repo.eventid));

            int iconAvatar;

            switch (repo.severity.toString().toUpperCase()) {
                case "MAJOR":
                    iconAvatar = R.drawable.major;
                    break;
                case "MINOR":
                    iconAvatar = R.drawable.minor;
                    break;
                default:
                    iconAvatar = R.drawable.critical;
                    break;
            }

            Picasso.with(imageAvatar.getContext())
                    .load(iconAvatar)
                    .resize(200, 200)
                    .error(R.drawable.ic_github_placeholder)
                    .placeholder(R.drawable.ic_github_placeholder)
                    .centerCrop()
                    .into(imageAvatar);
        }
    }
}
