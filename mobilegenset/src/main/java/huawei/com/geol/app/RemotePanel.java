package huawei.com.geol.app;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import huawei.com.geol.R;
import huawei.com.geol.mqtt.Constants;
import huawei.com.geol.mqtt.MqttMessageService;
import huawei.com.geol.mqtt.PahoMqttClient;
import huawei.com.geol.utils.HttpUtils;

import static huawei.com.geol.constant.constant.MY_PERMISSIONS_REQUEST_READ_PHONE_STATE;
import static huawei.com.geol.utils.ShareUtils.AlertMsg;

/**
 * Created by md101 on 10/23/16.
 */

public class RemotePanel extends AppCompatActivity {

    private MqttAndroidClient client;
    private String TAG = "RemotePanel";
    private PahoMqttClient pahoMqttClient;
    private ImageButton startengine, gensetauto, gensetman, gensetstop, transgenset, transmains, do1on, do1off, do2on, do2off;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mgenset_rpanel);
        pahoMqttClient = new PahoMqttClient();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        client = pahoMqttClient.getMqttClient(getApplicationContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setTitle("Auto Start Controller");

        gensetauto = (ImageButton) findViewById(R.id.imgbtnauto);
        gensetman = (ImageButton) findViewById(R.id.imgbtnman);
        gensetstop = (ImageButton) findViewById(R.id.imgbtnstop);
//        transgenset = (Button) findViewById(R.id.transgenset);
//        transmains = (Button) findViewById(R.id.transmains);
        startengine = (ImageButton) findViewById(R.id.imgbtnstart);

        gensetauto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                AlertMsg("information", "Auto Starting Genset.....",RemotePanel.this);
                String msg = "{ \"id\":\"test\", \"command\": \"<30\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        });

        gensetman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                AlertMsg("information", "Manual Starting Genset.....",RemotePanel.this);
                String msg = "{ \"id\":\"test\", \"command\": \"=30\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        });

        gensetstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                AlertMsg("information", "Stopping Genset.....",RemotePanel.this);
                String msg = "{ \"id\":\"test\", \"command\": \">30\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        });

        startengine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                AlertMsg("information", "Starting Genset.....",RemotePanel.this);
                String msg = "{ \"id\":\"test\", \"command\": \"<33\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        });

        Intent intent = new Intent(RemotePanel.this, MqttMessageService.class);
        startService(intent);
    }

//    public void SetMsgStart(View v) {
//        AlertMsg("information", "Starting Genset.....",this);
//        String msg = "{ \"id\":\"test\", \"command\": \"<33\", \"aux\":\"Texas\"}";
//        if (!msg.isEmpty()) {
//            try {
//                pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
//            } catch (MqttException e) {
//                e.printStackTrace();
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        }
//    }

//    public void SetMsgMenu(View v) {
//        AlertMsg("information", "Menu Genset.....",this);
//    }
//
//    public void SetMsgTestLoad(View v) {
//        AlertMsg("information", "Test Load Genset.....",this);
//    }

//    public void SetMsgAuto(View v) {
//        AlertMsg("information", "Auto Starting Genset.....",this);
//        //String msg = textMessage.getText().toString().trim();
//        String msg = "{ \"id\":\"test\", \"command\": \"<30\", \"aux\":\"Texas\"}";
//        if (!msg.isEmpty()) {
//            try {
//                pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
//            } catch (MqttException e) {
//                e.printStackTrace();
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public void SetMsgMan(View v) {
//        AlertMsg("information", "Manual Starting Genset.....",this);
//        String msg = "{ \"id\":\"test\", \"command\": \"=30\", \"aux\":\"Texas\"}";
//        if (!msg.isEmpty()) {
//            try {
//                pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
//            } catch (MqttException e) {
//                e.printStackTrace();
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public void SetMsgStop(View v) {
//        AlertMsg("information", "Stopping Genset.....",this);
//        String msg = "{ \"id\":\"test\", \"command\": \">30\", \"aux\":\"Texas\"}";
//        if (!msg.isEmpty()) {
//            try {
//                pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
//            } catch (MqttException e) {
//                e.printStackTrace();
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        }
//    }
}
