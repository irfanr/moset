package huawei.com.geol.app;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.stephentuso.welcome.WelcomeScreenHelper;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.MenuParams;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemLongClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import huawei.com.geol.R;
import huawei.com.geol.adapter.ParentAdapter;
import huawei.com.geol.custom.CustomViewPager;

public class MainNew extends AppCompatActivity implements
        OnMenuItemClickListener, OnMenuItemLongClickListener {
    private ContextMenuDialogFragment mMenuDialogFragment;
    WelcomeScreenHelper welcomeScreen;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainnew);

        int [] icontab = {
//                R.drawable.ic_chart_24dp,
//                R.drawable.ic_alarm_help,
                R.mipmap.ic_launcher
        };

        CustomViewPager vp = (CustomViewPager) findViewById(R.id.viewpager);
        TabLayout tl = (TabLayout) findViewById(R.id.tab_layout);

        ParentAdapter adapter = new ParentAdapter(getSupportFragmentManager(), new ArrayList<>(Arrays.asList("")));

        vp.setPaging(false);
        vp.setAdapter(adapter);
        vp.setOffscreenPageLimit(1);
        tl.setupWithViewPager(vp);
tl.removeTabAt(0);
//        for (int i = 0; i < tl.getTabCount(); i++) {
//            tl.getTabAt(i).setIcon(icontab[i]);
//        }

//        tl.setTabTextColors(R.color.saffron,R.color.colorPrimaryDark);

        welcomeScreen = new WelcomeScreenHelper(this, Welcome.class);
        welcomeScreen.show(savedInstanceState);

        fragmentManager = getSupportFragmentManager();

        initToolbar();
        initMenuFragment();
    }

    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(getMenuObjects());
        menuParams.setClosableOutside(false);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(this);
        mMenuDialogFragment.setItemLongClickListener(this);
    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {
        if (position == 1) {
            welcomeScreen.forceShow();
        }
    }

    @Override
    public void onMenuItemLongClick(View clickedView, int position) {
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.text_view_toolbar_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolBarTextView.setText(R.string.app_title);
    }

    private List<MenuObject> getMenuObjects() {

        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();
        close.setResource(R.drawable.ic_close_24dp);

        MenuObject help = new MenuObject("Help");
        help.setResource(R.drawable.ic_help_24dp);

        menuObjects.add(close);
        menuObjects.add(help);

        return menuObjects;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
