package huawei.com.geol.app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import huawei.com.geol.R;

/**
 * Created by md101 on 9/4/16.
 */

public class MainNEDownGIS extends Fragment implements
        SearchNEDown.onSubmitListener {
    public MainNEDownGIS() {
        // Required empty public constructor
    }

    public static MainNEDownGIS newInstance() {
        return new MainNEDownGIS();
    }

    private static final String TAG = "MainNeDownGIS.class";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.ne_down_gis, container, false);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchNEDown fragment1 = new SearchNEDown();
                fragment1.mListener = MainNEDownGIS.this;
                fragment1.show(getActivity().getFragmentManager(), "");
            }
        });

        if (savedInstanceState == null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_nedowngis, new NeDownFragmentGIS())
                    .commit();
        }
        return rootView;
    }

    @Override
    public void setOnSubmitListener(String region, String siteid, String manager, String spv) {
        Bundle args = new Bundle();
        args.putString("region", region);
        args.putString("site", siteid);
        args.putString("manager", manager);
        args.putString("spv", spv);
        NeDownFragmentGIS neDownFragmentGIS = new NeDownFragmentGIS();
        neDownFragmentGIS.setArguments(args);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_nedowngis, neDownFragmentGIS)
                .commit();
    }
}
