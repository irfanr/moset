package huawei.com.geol.app;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableWeightLayout;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import huawei.com.geol.R;
import huawei.com.geol.utils.HttpUtils;

import static huawei.com.geol.constant.constant.MY_PERMISSIONS_REQUEST_READ_PHONE_STATE;
import static huawei.com.geol.utils.ShareUtils.AlertMsg;

/**
 * Created by md101 on 10/23/16.
 */

public class SiteEntry extends AppCompatActivity {
    AsyncHttpClient client = new AsyncHttpClient();
    Spinner spinner_remarks;
    EditText userinput, siteid;
    String urlremarks = "/netcool/getalarmremarkcat";
    TextView siteiddata, sitenamedata, sitetechdata, siteproviderdata;
    private ProgressBar loadingbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.site_entry);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        spinner_remarks = (Spinner) findViewById(R.id.spinn_alarm_cat);
        userinput = (EditText) findViewById(R.id.editTextDialogUserInput);
        siteid = (EditText) findViewById(R.id.txt_SiteID);
        siteiddata = (TextView) findViewById(R.id.view_SiteID_data);
        sitenamedata = (TextView) findViewById(R.id.view_SiteName_data);
        sitetechdata = (TextView) findViewById(R.id.view_SiteTech_data);
        siteproviderdata = (TextView) findViewById(R.id.view_SiteProvider_data);
        loadingbar = (ProgressBar) findViewById(R.id.progressBar1);

        loadingbar.setVisibility(View.GONE);

        setTitle("Site Entry");
        InitRemarkCategory();
    }

    private boolean GrantPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SiteEntry.this);

                alertDialogBuilder.setTitle("Confirmation");
                alertDialogBuilder.setMessage("m-GeOL need request phone state");

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        ActivityCompat.requestPermissions(SiteEntry.this,
                                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                                MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                                        dialog.cancel();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Already Grant Permission
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void InitRemarkCategory() {
        final ArrayList<String> remarks_cat = new ArrayList<>();

        client.post(HttpUtils.getAbsoluteUrlPhp(urlremarks), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                for (int x = 0; x < response.length(); x++) {
                    try {
                        JSONObject data = response.getJSONObject(x);
                        remarks_cat.add(data.getString("remark_category"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(response);

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                        getApplicationContext(),
                        R.layout.layout_remarks,
                        remarks_cat);
                dataAdapter.setDropDownViewResource(R.layout.listremarkcats);
                spinner_remarks.setAdapter(dataAdapter);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(throwable);
            }
        });
    }

    public void SetRemarks(View v) {
        if (GrantPermission()) {
            if (siteid.getText().toString().isEmpty() || siteid.getText().toString().equalsIgnoreCase("") ||
                    siteiddata.getText().toString().equalsIgnoreCase("no data")) {
                AlertMsg("Warning", "Please fill Site ID", this);
                siteid.requestFocus();
            } else if (userinput.getText().toString().isEmpty() || userinput.getText().toString().equalsIgnoreCase("")) {
                AlertMsg("Warning", "Please fill Remark", this);
                userinput.requestFocus();
            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        this);

                alertDialogBuilder.setTitle("Confirmation");
                alertDialogBuilder.setMessage("Are you sure want to submit?");

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        SaveRemark(
                                                spinner_remarks.getSelectedItem().toString(),
                                                GetIMEI(),
                                                siteid.getText().toString(),
                                                userinput.getText().toString());
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        } else {
            AlertMsg("information", "m-GeOL need request phone state",this);
        }
    }

    private String GetIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public void GetSite(View s) {
        setDataNull();
        if (siteid.getText().toString().isEmpty() || siteid.getText().toString().equalsIgnoreCase("")) {
            AlertMsg("Warning", "Please fill Site ID", this);
            siteid.requestFocus();
        } else {
            loadSite(siteid.getText().toString());
        }
    }

    public void SaveRemark(String cat, String remarker, String siteid, String remark) {
        String url = "/netcool/save_remark_m2";
        final RequestParams params = new RequestParams();
        params.add("category", cat);
        params.add("remarker", remarker.toLowerCase());
        params.add("siteid", siteid);
        params.add("remark", remark);

        System.out.println(remarker.toLowerCase());

        client.post(HttpUtils.getAbsoluteUrlPhp(url), params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                System.out.println(response);
                System.out.println(params);
                try {
                    String returndata = response.getString("status");
                    Toast.makeText(getApplicationContext(), returndata, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(params);
                System.out.println(throwable);
            }
        });
    }

    private void loadSite(final String site) {
        new AsyncTask<Integer, Void, List<APIClient.Site>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadingbar.setVisibility(View.VISIBLE);
            }

            @Override
            protected List<APIClient.Site> doInBackground(Integer... params) {
                try {
                    return APIClient.getJavaClient()
                            .getSite(site.toUpperCase()).sites;
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<APIClient.Site> repos) {
                if (repos == null) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Error getting data Site...",
                            Toast.LENGTH_LONG).show();
                } else {
                    SetSite(repos);
                }
                loadingbar.setVisibility(View.GONE);
            }
        }.execute(1);
    }

    private void setDataNull() {
        siteiddata.setText("");
        sitenamedata.setText("");
        siteproviderdata.setText("");
        sitetechdata.setText("");
    }

    private void SetSite(List<APIClient.Site> repos) {
        for (Iterator<APIClient.Site> iter = repos.iterator(); iter.hasNext();) {
            APIClient.Site repo = iter.next();
            siteiddata.setText(repo.siteid);
            sitenamedata.setText(repo.sitename);
            siteproviderdata.setText(repo.tower_provider);
            sitetechdata.setText(String.valueOf(repo.technology_id));
        }
    }
}
