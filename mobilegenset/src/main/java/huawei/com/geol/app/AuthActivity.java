package huawei.com.geol.app;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;

import huawei.com.geol.R;

import static huawei.com.geol.constant.constant.MY_PERMISSIONS_REQUEST_READ_PHONE_STATE;

public class AuthActivity extends AppCompatActivity {
    String IMEI = "";
    Boolean result = false;
    CountDownTimer timer;
    ProgressDialog progressdialog;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        intent = new Intent(this, MainMenu.class);
        intent.addFlags(intent.FLAG_ACTIVITY_NO_ANIMATION);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        // Here, thisActivity is the current activity
//        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
//                Manifest.permission.READ_PHONE_STATE)
//                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.READ_PHONE_STATE)) {
//
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AuthActivity.this);
//
//                alertDialogBuilder.setTitle("Confirmation");
//                alertDialogBuilder.setMessage("m-Genset need request phone state");
//
//                alertDialogBuilder
//                        .setCancelable(false)
//                        .setPositiveButton("OK",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        ActivityCompat.requestPermissions(AuthActivity.this,
//                                                new String[]{Manifest.permission.READ_PHONE_STATE},
//                                                MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
//                                        dialog.cancel();
//                                    }
//                                })
//                        .setNegativeButton("Cancel",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.cancel();
//                                        finish();
//                                    }
//                                });
//
//                AlertDialog alertDialog = alertDialogBuilder.create();
//                alertDialog.show();
//            } else {
//
//                // No explanation needed, we can request the permission.
//
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission.READ_PHONE_STATE},
//                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
//        } else {
//            GetIMEI();
//            AuthIMEI();
            startActivity(intent);
            finish();
//        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    GetIMEI();
//                    AuthIMEI();
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    finish();
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }

//    private void GetIMEI() {
//        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        IMEI = telephonyManager.getDeviceId();
//    }

//    private void AuthIMEI() {
//        progressdialog = ProgressDialog.show(this,
//                getString(R.string.validation), getString(R.string.WaitingText));
//
//        timer = new CountDownTimer(20000, 1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                progressdialog.setMessage(String.valueOf(millisUntilFinished / 1000));
//            }
//
//            @Override
//            public void onFinish() {
//                if (!result) {
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(AuthActivity.this);
//                    builder.setMessage("Cannot Check IMEI, please contact GeOL Admin");
//                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            finish();
//                        }
//                    });
//                    progressdialog.dismiss();
//                    builder.show();
//                } else {
//                    progressdialog.dismiss();
//                }
//            }
//        };
//
//        timer.start();
//        CheckIMEI(IMEI);
//    }

//    private void CheckIMEI(final String data) {
//        new AsyncTask<Integer, Void, String>() {
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            protected String doInBackground(Integer... params) {
//                try {
//                    return APIClient
//                            .getJavaClient()
//                            .authimei(data)
//                            .user;
//                } catch (Exception ex) {
//                    return "";
//                }
//            }
//
//            @Override
//            protected void onPostExecute(String repos) {
//                if (repos.isEmpty() || repos.equalsIgnoreCase("") || repos == null || repos.equalsIgnoreCase("0")) {
//                } else {
//                    result = true;
//                    progressdialog.dismiss();
//                    timer.cancel();
//                    startActivity(intent);
//                    finish();
//                }
//            }
//        }.execute(1);
//    }
}
