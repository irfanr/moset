package huawei.com.geol.app;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import huawei.com.geol.R;

public class NeDownFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    interface OnLoadingListener {
        void onLoadingStarted();

        void onLoadingFinished();
    }

    private String query = "android";
    private String language = "java";
    private String queryString = "%s+language:%s";
    final int DEFAULT_PER_PAGE = 10;

    private BaseAttacher mBaseAttacher;
    int currentPage = 1;
    int perPage = DEFAULT_PER_PAGE;
    boolean isLoading = false;

    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    RepoAdapter mRepoAdapter;
    OnLoadingListener mListener;

    ProgressBar mProgressBarParent;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NeDownFragment.OnLoadingListener) {
            mListener = (OnLoadingListener) context;
        }
        mProgressBarParent = (ProgressBar) getActivity().findViewById(R.id.progressBarNeDown);
    }

    public void onLoadingStarted() {
        mProgressBarParent.setVisibility(View.VISIBLE);
    }

    public void onLoadingFinished() {
        mProgressBarParent.setVisibility(View.INVISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mProgressBarParent = (ProgressBar) getActivity().findViewById(R.id.progressBarNeDown);

        View rootView = inflater.inflate(R.layout.fragment_ne_down, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewNeDown);

        registerForContextMenu(mRecyclerView);

        LinearLayoutManager manager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL,
                false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mRepoAdapter = new RepoAdapter());

        perPage = getPerPage(rootView.getContext());

        loadData(currentPage, perPage);
        return rootView;
    }

    /**
     * Get items to load per page onScroll.
     *
     * @param context {@link Context}
     * @return int of num of items that can be loaded onto the screen with scroll enabled
     */
    private int getPerPage(Context context) {
        //fixed item size in recyclerview. Adding 3 enables recyclerview scrolling.
        return (context.getResources().getDisplayMetrics().heightPixels
                / context.getResources().getDimensionPixelSize(R.dimen.repo_item_height)) + 3;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBaseAttacher = Mugen.with(mRecyclerView, new MugenCallbacks() {
            @Override
            public void onLoadMore() {
                loadData(currentPage + 1, perPage);
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return false;
            }
        }).start();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
//        Toast.makeText(getContext(), "Halo", Toast.LENGTH_LONG).show();
        return super.onContextItemSelected(item);
    }

    private void loadData(final int page, final int perPage) {
        new AsyncTask<Integer, Void, List<APIClient.NEDown>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                onLoadingStarted();
            }

            @Override
            protected List<APIClient.NEDown> doInBackground(Integer... params) {
                String q = String.format(Locale.ENGLISH,
                        queryString,
                        query,
                        language);
                isLoading = true;
                return APIClient.getJavaClient()
                        .searchnedown().nedown;
            }

            @Override
            protected void onPostExecute(List<APIClient.NEDown> repos) {
                isLoading = false;
                if (repos != null) {
                    mRepoAdapter.onNext(repos, page);
                }

                onLoadingFinished();

                currentPage = page;
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }.execute(page);
    }

    @Override
    public void onRefresh() {
        loadData(1, perPage);
    }

    private static class RepoAdapter extends RecyclerView.Adapter<RepoHolder> implements View.OnCreateContextMenuListener {

        LinkedHashMap<Integer, List<APIClient.NEDown>> repoMap;
        List<APIClient.NEDown> repoList;

        private int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public RepoAdapter() {
            repoMap = new LinkedHashMap<Integer, List<APIClient.NEDown>>();
            repoList = new ArrayList<APIClient.NEDown>();
        }

        @Override
        public RepoHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_ne_down, viewGroup, false);
            view.setOnCreateContextMenuListener(this);
            view.setClickable(true);

            return new RepoHolder(view);
        }

        @Override
        public void onViewRecycled(RepoHolder holder) {
            holder.itemView.setOnLongClickListener(null);
            super.onViewRecycled(holder);
        }

        @Override
        public void onBindViewHolder(RepoHolder repoHolder, int i) {
            APIClient.NEDown repo = getItem(i);
            if (repo == null) {
                return;
            }

            repoHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });

            repoHolder.loadRepo(repo);
        }

        private APIClient.NEDown getItem(int position) {
            int listSize = 0;

            if (repoList.size() > position) {
                return repoList.get(position);
            }

            repoList = new ArrayList<APIClient.NEDown>();
            for (List<APIClient.NEDown> list : repoMap.values()) {
                repoList.addAll(list);
                listSize = listSize + list.size();
                if (listSize > position) {
                    break;
                }
            }
            if (repoList.size() > 0) {
                return repoList.get(position);
            }
            return null;
        }

        @Override
        public int getItemCount() {
            int count = 0;
            for (List<APIClient.NEDown> list : repoMap.values()) {
                count = count + list.size();
            }
            return count;
        }

        public void onNext(List<APIClient.NEDown> repos, int page) {
            if (repos == null) {
                return;
            }
            repoMap.put(page, repos);
            notifyDataSetChanged();
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(0, v.getId(), 0, "View Details NE");
        }
    }

    private static class RepoHolder extends RecyclerView.ViewHolder {

        ImageView imageAvatar;
        TextView site;
        TextView node;
        TextView siteid;
        TextView ttno;
        TextView occurrence;

        public RepoHolder(View itemView) {
            super(itemView);
            imageAvatar = (ImageView) itemView.findViewById(R.id.imageView_avatar);
            site = (TextView) itemView.findViewById(R.id.textView_site);
            node = (TextView) itemView.findViewById(R.id.textView_node);
            siteid = (TextView) itemView.findViewById(R.id.textView_siteID);
            ttno = (TextView) itemView.findViewById(R.id.textView_TTNO);
            occurrence = (TextView) itemView.findViewById(R.id.textView_occurrence);

            ((ImageView) itemView.findViewById(R.id.imageView_triangle)).
                    setColorFilter(itemView
                            .getContext()
                            .getResources()
                            .getColor(R.color.blue_light));
        }

        public void loadRepo(APIClient.NEDown repo) {
            site.setText(repo.site);
            node.setText(repo.node);
            siteid.setText(repo.siteid);
            ttno.setText(repo.ttno);
            occurrence.setText(repo.firstOccurrence);

            int iconAvatar;

            switch (repo.ne.toString().toUpperCase()) {
                case "2G":
                    iconAvatar = R.drawable.ic_2g;
                    break;
                case "3G":
                    iconAvatar = R.drawable.ic_3g;
                    break;
                default:
                    iconAvatar = R.drawable.ic_4g;
                    break;
            }

            Picasso.with(imageAvatar.getContext())
                    .load(iconAvatar)
                    .resize(200, 200)
                    .error(R.drawable.ic_github_placeholder)
                    .placeholder(R.drawable.ic_github_placeholder)
                    .centerCrop()
                    .into(imageAvatar);
        }
    }
}
