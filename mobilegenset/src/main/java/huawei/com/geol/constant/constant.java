package huawei.com.geol.constant;

import huawei.com.geol.R;

import static android.provider.Settings.Global.getString;

/**
 * Created by md101 on 8/13/16.
 */

public class constant {
    public static String LOG_TAG;
    public static final String BASE_URL = "http://10.9.12.162:8081/geolws/";
    public static final String BASE_URL_PHP = "http://116.66.203.240:8889/pm5/index.php";
    public static final String BASE_URL_JAVA = "http://116.66.203.240:8888/geolws";
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 2;
}
