package huawei.com.geol.utils;

import static huawei.com.geol.constant.constant.BASE_URL;
import static huawei.com.geol.constant.constant.BASE_URL_PHP;

/**
 * Created by md101 on 12/7/15.
 */
public class HttpUtils {

    public static String getAbsoluteUrlJava(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    public static String getAbsoluteUrlPhp(String relativeUrl) {
        return BASE_URL_PHP + relativeUrl;
    }

//    public static String getAbsoluteUrlPhp_10_9_216_20(String relativeUrl) {
//        return constant.BASE_URL_PHP_10_9_216_20 + relativeUrl;
//    }

}
