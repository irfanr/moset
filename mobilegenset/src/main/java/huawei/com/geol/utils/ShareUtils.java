package huawei.com.geol.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.telephony.TelephonyManager;

/**
 * Created by md101 on 10/17/16.
 */

public class ShareUtils {

    public static void AlertMsg(String titleBox, String Msg, Context context) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
        dlgAlert.setTitle(titleBox);
        dlgAlert.setMessage(Msg);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    public static boolean isNumeric(String s) {
        return s.matches("[-+]?\\d*\\.?\\d+");
    }

}
