package com.app.androidkt.mqtt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {


    private MqttAndroidClient client;
    private String TAG = "MainActivity";
    private PahoMqttClient pahoMqttClient;

    private EditText textMessage, subscribeTopic, unSubscribeTopic;
    private Button startengine, gensetauto, gensetman, gensetstop, transgenset, transmains, do1on, do1off, do2on, do2off;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pahoMqttClient = new PahoMqttClient();

        //textMessage = (EditText) findViewById(R.id.textMessage);
        do2on = (Button) findViewById(R.id.do2on);
        do2off = (Button) findViewById(R.id.do2off);
        //unSubscribe = (Button) findViewById(R.id.unSubscribe);

        do1on = (Button) findViewById(R.id.do1on);
        do1off = (Button) findViewById(R.id.do1off);

        gensetauto = (Button) findViewById(R.id.gensetauto);
        gensetman = (Button) findViewById(R.id.gensetman);
        gensetstop = (Button) findViewById(R.id.gensetstop);
        transgenset = (Button) findViewById(R.id.transgenset);
        transmains = (Button) findViewById(R.id.transmains);
        startengine = (Button) findViewById(R.id.startengine);



        //subscribeTopic = (EditText) findViewById(R.id.subscribeTopic);
        //unSubscribeTopic = (EditText) findViewById(R.id.unSubscribeTopic);
        client = pahoMqttClient.getMqttClient(getApplicationContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);

        gensetauto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"test\", \"command\": \"<30\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        gensetman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"test\", \"command\": \"=30\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        gensetstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"test\", \"command\": \">30\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        transgenset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"test\", \"command\": \">33\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        transmains.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"test\", \"command\": \"=33\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        startengine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"test\", \"command\": \"<33\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

/*
        publishMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = textMessage.getText().toString().trim();
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String topic = subscribeTopic.getText().toString().trim();
                if (!topic.isEmpty()) {
                    try {
                        pahoMqttClient.subscribe(client, topic, 1);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        unSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String topic = unSubscribeTopic.getText().toString().trim();
                if (!topic.isEmpty()) {
                    try {
                        pahoMqttClient.unSubscribe(client, topic);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
*/
        do1on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"do1\", \"command\": \"1\", \"aux\":\"New York\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        do1off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"do1\", \"command\": \"0\", \"aux\":\"New York\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        do2on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"do2\", \"command\": \"1\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        do2off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String msg = textMessage.getText().toString().trim();
                String msg = "{ \"id\":\"do2\", \"command\": \"0\", \"aux\":\"Texas\"}";
                if (!msg.isEmpty()) {
                    try {
                        pahoMqttClient.publishMessage(client, msg, 1, Constants.PUBLISH_TOPIC);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Intent intent = new Intent(MainActivity.this, MqttMessageService.class);
        startService(intent);
    }
}
